﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapCreatorUtility.Helpers
{
    public static class Searcher
    {
        /// <summary>
        /// Allows to get all components of T type in sphere area.
        /// </summary>
        public static T[] GetComponentsInSphereArea<T>(Vector3 position, float radius, LayerMask layer, QueryTriggerInteraction query = QueryTriggerInteraction.UseGlobal)
        {
            bool initialQueries = Physics.queriesHitTriggers;

            Physics.queriesHitTriggers = true;

            Collider[] Colliders = Physics.OverlapSphere(position, radius, layer, query);

            Physics.queriesHitTriggers = initialQueries;

            List<T> Types = new List<T>();

            for (int i = 0; i < Colliders.Length; i++)
            {
                T Type = Colliders[i].GetComponentInParent<T>();

                if (Type != null)
                {
                    if (Type is T)
                    {
                        if (!Types.Contains(Type))
                            Types.Add(Type);
                    }
                }
            }

            return Types.ToArray();
        }

        /// <summary>
        /// Allows to get all components of T type in box area.
        /// </summary>
        public static T[] GetComponentsInBoxArea<T>(Vector3 position, Vector3 size, Quaternion rotation, LayerMask layer, QueryTriggerInteraction query = QueryTriggerInteraction.UseGlobal)
        {
            bool initialQueries = Physics.queriesHitTriggers;

            Physics.queriesHitTriggers = true;

            Collider[] colliders = Physics.OverlapBox(position, size, rotation, layer, query);

            Physics.queriesHitTriggers = initialQueries;

            List<T> types = new List<T>();

            for (int i = 0; i < colliders.Length; i++)
            {
                T component = colliders[i].GetComponentInParent<T>();

                if (component != null)
                {
                    if (component is T)
                        if (!types.Contains(component))
                            types.Add(component);
                }
            }

            return types.ToArray();
        }
    }
}
