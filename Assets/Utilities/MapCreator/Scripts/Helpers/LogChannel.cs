﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapCreatorUtility.Helpers
{
    public class LogChannel
    {
        private readonly string logName;

        public LogChannel(string logName)
        {
            this.logName = logName;
        }

        public void LogInformation(string message)
        {
            Debug.Log("<b><color=green>" + logName + "</color></b> : " + message);
        }

        public void LogWarning(string message)
        {
            Debug.Log("<b><color=yellow>" + logName + "</color></b> : " + message);
        }

        public void LogError(string message)
        {
            Debug.Log("<b><color=red>" + logName + "</color></b> : " + message);
        }
    }
}
