﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapCreatorUtility.Helpers
{
    public static class Layers
    {
        /// <summary>
        /// This method allows to get index from a bitmask (LayerMask).
        /// </summary>
        public static int ToLayerIndex(int bitmask)
        {
            int Result = bitmask > 0 ? 0 : 31;

            while (bitmask > 1)
            {
                bitmask = bitmask >> 1;

                Result++;
            }

            return Result;
        }
    }
}
