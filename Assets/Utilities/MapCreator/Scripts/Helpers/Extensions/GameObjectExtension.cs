﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapCreatorUtility.Helpers.Extensions
{
    public static class GameObjectExtension
    {
        /// <summary>
        /// Allows to add a rigidbody component to a gameObject.
        /// </summary>
        public static void AddRigibody(this GameObject target, bool useGravity, bool isKinematic, float maxDepenetrationVelocity = 15f, HideFlags flag = HideFlags.HideAndDontSave)
        {
            if (target == null)
                return;

            if (target.GetComponent<Rigidbody>() != null)
                return;

            Rigidbody component = target.AddComponent<Rigidbody>();
            component.maxDepenetrationVelocity = maxDepenetrationVelocity;
            component.useGravity = useGravity;
            component.isKinematic = isKinematic;
            component.hideFlags = flag;
        }

        /// <summary>
        /// This allows to add a sphere collider component to a gameObject.
        /// </summary>
        public static void AddSphereCollider(this GameObject target, float radius, bool isTrigger = true, HideFlags flag = HideFlags.HideAndDontSave)
        {
            if (target == null)
                return;

            if (target.GetComponent<Rigidbody>() != null)
                return;

            SphereCollider component = target.AddComponent<SphereCollider>();
            component.radius = radius;
            component.isTrigger = isTrigger;
            component.hideFlags = flag;
        }

        /// <summary>
        /// This allows to add a box collider component to a gameObject.
        /// </summary>
        public static void AddBoxCollider(this GameObject target, Vector3 size, Vector3 center, bool isTrigger = true, HideFlags flag = HideFlags.HideAndDontSave)
        {
            if (target == null)
                return;

            if (target.GetComponent<Rigidbody>() != null)
                return;

            BoxCollider component = target.AddComponent<BoxCollider>();
            component.size = size;
            component.center = center;
            component.isTrigger = isTrigger;
            component.hideFlags = flag;
        }

        /// <summary>
        /// This method allows of change recursively all layers of each transform child.
        /// </summary>
        public static void SetLayerRecursively(this GameObject go, LayerMask layer)
        {
            if (go == null)
            {
                return;
            }

            go.layer = Layers.ToLayerIndex(layer.value);

            foreach (Transform child in go.transform)
            {
                if (child == null)
                {
                    continue;
                }

                SetLayerRecursively(child.gameObject, layer);
            }
        }

        /// <summary>
        /// This allows to change all the materials color of childrens.
        /// </summary>
        public static void ChangeAllMaterialsColorInChildren(this GameObject go, Renderer[] renderers, Color color, float lerpTime = 15.0f, bool lerp = false)
        {
            Renderer[] Renderers = go.GetComponentsInChildren<Renderer>();

            for (int i = 0; i < Renderers.Length; i++)
            {
                if (Renderers[i] != null)
                {
                    for (int x = 0; x < Renderers[i].materials.Length; x++)
                    {
                        if (lerp)
                            Renderers[i].materials[x].color = Color.Lerp(Renderers[i].materials[x].color, color, lerpTime * Time.deltaTime);
                        else
                            Renderers[i].materials[x].color = color;
                    }
                }
            }
        }

        /// <summary>
        /// This allows to change all the materials of childrens.
        /// </summary>
        public static void ChangeAllMaterialsInChildren(this GameObject go, Renderer[] renderers, Material material)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                if (renderers[i] != null)
                {
                    Material[] materials = new Material[renderers[i].sharedMaterials.Length];

                    for (int x = 0; x < renderers[i].sharedMaterials.Length; x++)
                        materials[x] = material;

                    renderers[i].sharedMaterials = materials;
                }
            }
        }

        /// <summary>
        /// This allows to change all the materials of childrens (used for the restoration of initial materials).
        /// </summary>
        public static void ChangeAllMaterialsInChildren(this GameObject go, Renderer[] renderers, Dictionary<Renderer, Material[]> materials)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                Material[] CacheMaterials = renderers[i].sharedMaterials;

                for (int c = 0; c < CacheMaterials.Length; c++)
                    CacheMaterials[c] = materials[renderers[i]][c];

                renderers[i].materials = CacheMaterials;
            }
        }
    }
}
