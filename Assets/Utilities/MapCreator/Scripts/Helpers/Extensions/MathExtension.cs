﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapCreatorUtility.Helpers.Extensions
{

    public static class MathExtension
    {
        #region Public Methods

        /// <summary>
        /// This method allows to encapsuled all the childs and return the result bounds.
        /// </summary>
        public static Bounds GetChildsBounds(this GameObject target)
        {
            MeshRenderer[] renderers = target.GetComponentsInChildren<MeshRenderer>();

            Quaternion currentRotation = target.transform.rotation;

            Vector3 currentScale = target.transform.localScale;

            target.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

            target.transform.localScale = Vector3.one;

            Bounds resultBounds = new Bounds(target.transform.position, Vector3.zero);

            foreach (Renderer renderer in renderers)
                resultBounds.Encapsulate(renderer.bounds);

            Vector3 relativeCenter = resultBounds.center - target.transform.position;

            resultBounds.center = PositionToGridPosition(0.1f, 0f, relativeCenter);

            resultBounds.size = PositionToGridPosition(0.1f, 0f, resultBounds.size);

            target.transform.rotation = currentRotation;

            target.transform.localScale = currentScale;

            return resultBounds;
        }

        /// <summary>
        /// This method allows to encapsuled the parent and return the result bounds.
        /// </summary>
        public static Bounds GetParentBounds(this GameObject target)
        {
            MeshRenderer[] renderers = target.GetComponents<MeshRenderer>();

            Quaternion currentRotation = target.transform.rotation;

            Vector3 currentScale = target.transform.localScale;

            target.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

            target.transform.localScale = Vector3.one;

            Bounds resultBounds = new Bounds(target.transform.position, Vector3.zero);

            foreach (Renderer renderer in renderers)
                resultBounds.Encapsulate(renderer.bounds);

            Vector3 relativeCenter = resultBounds.center - target.transform.position;

            resultBounds.center = PositionToGridPosition(0.1f, 0f, relativeCenter);

            resultBounds.size = PositionToGridPosition(0.1f, 0f, resultBounds.size);

            target.transform.rotation = currentRotation;

            target.transform.localScale = currentScale;

            return resultBounds;
        }

        /// <summary>
        /// This allows to change the local space of bounds to world space.
        /// </summary>
        public static Bounds BoundsToWorld(this Transform transform, Bounds localBounds)
        {
            if (transform != null)
                return new Bounds(transform.TransformPoint(localBounds.center), localBounds.size);
            else
                return new Bounds(localBounds.center, localBounds.size);
        }

        /// <summary>
        /// This allows to get an axis in a grid.
        /// </summary>
        public static float ConvertToGrid(float gridSize, float gridOffset, float axis)
        {
            return Mathf.Round(axis) * gridSize + gridOffset;
        }

        /// <summary>
        /// This allows to get a vector in a grid.
        /// </summary>
        public static Vector3 PositionToGridPosition(float gridSize, float gridOffset, Vector3 position)
        {
            position -= Vector3.one * gridOffset;
            position /= gridSize;
            position = new Vector3(Mathf.Round(position.x), Mathf.Round(position.y), Mathf.Round(position.z));
            position *= gridSize;
            position += Vector3.one * gridOffset;
            return position;
        }

        #endregion Public Methods
    }
}
