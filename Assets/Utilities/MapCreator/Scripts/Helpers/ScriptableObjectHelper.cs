﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MapCreatorUtility.Helpers
{
    public static class ScriptableObjectHelper
    {
        // #region Public Class

        // internal class EndNameEdit : EndNameEditAction
        // {
        //     public override void Action(int instanceId, string pathName, string resourceFile)
        //     {
        //         AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(instanceId), AssetDatabase.GenerateUniqueAssetPath(pathName));
        //     }
        // }

        // #endregion Public Class

        public static T CreateAsset<T>(string path, string name, bool select = true) where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();

            AssetDatabase.CreateAsset(asset, path + name + ".asset");

            AssetDatabase.SaveAssets();

            if (select)
            {
                EditorUtility.FocusProjectWindow();

                Selection.activeObject = asset;

                EditorGUIUtility.PingObject(asset);
            }

            return asset;
        }

        public static T LoadAsset<T>(string resourcesPath, string name) where T : ScriptableObject{
            return Resources.Load<T>(resourcesPath + name);
        }
    }
}
