﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MapCreatorUtility.Runtimes.Parts;
using MapCreatorUtility.Helpers;

namespace MapCreatorUtility.Editor.Parts
{
    [CustomEditor(typeof(Part))]
    public class PartInspector : UnityEditor.Editor
    {
        private LogChannel logChannel = new LogChannel("PartInspector");
        private Part part;

        private void Awake(){
        }

        private void OnEnable(){
            part = (Part)target;
        }

		private void OnDisable(){
            logChannel.LogInformation("Part disabled!");
		}

		private void OnDestroy(){
            if (Application.isEditor)
            {
                if (target == null)
                {
                    logChannel.LogInformation("Part destroyed! part " + part);
                }
            }
        }
    }
}
