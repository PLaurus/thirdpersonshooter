﻿using System;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;
using MapCreatorUtility.Runtimes.MainBuilder;
using MapCreatorUtility.Editor.UI;
using MapCreatorUtility.Runtimes.Events;
using MapCreatorUtility.Editor.Listeners;

namespace MapCreatorUtility.Editor.Initializer
{

    [InitializeOnLoad]
    static class MapCreatorInitializer
    {
        static MapCreatorInitializer()
        {
            MapCreatorEvents.onMapCreatorEnabled += MapCreatorUI.GetInstance().Initialize;
            MapCreatorEvents.onMapCreatorDisabled += MapCreatorUI.GetInstance().Uninitialize;
            MapCreator.StartIfEnabled();
            PlayModeStateListener.onEnterEditMode += MapCreator.StartIfEnabled;
            PlayModeStateListener.onEnterPlayMode += MapCreator.CloseIfEnabled;
        }

    }

}
