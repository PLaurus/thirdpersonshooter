﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Helpers;
using MapCreatorUtility;
using MapCreatorUtility.Runtimes.MainBuilder;
using MapCreatorUtility.Runtimes.Managers;
using MapCreatorUtility.Runtimes.Parts;
using MapCreatorUtility.Runtimes.Parts.Data;
using UnityEditor;
using UnityEngine;

namespace MapCreatorUtility.Editor.Windows
{
    public class MapCreatorWindow : EditorWindow
    {
        private int activeTab = 0;
        private LogChannel logChannel = new LogChannel("MapCreatorWindow");

        #region PartsCollection Fields

        private Part prefabToAdd;

        private PartsCollection PartsCollection
        {
            get
            {
                return MapCreatorManager.GetInstance().PartsCollection;
            }
        }

        private Vector2 ScrollPosition;

        #endregion PartsCollection Fields

        public static void OpenWindow()
        {
            GetWindow(typeof(MapCreatorWindow));
        }

        private void OnInspectorUpdate()
        {
            Repaint();
        }

        private void OnGUI()
        {
            if (!MapCreator.IsEnabled())
            {
                GUILayout.Label("Map creator is disabled...");
                return;
            }

            EditorGUILayout.Vector3Field("Camera position:", MapCreator.GetInstance().getEditorCameraPosition());

            activeTab = GUILayout.Toolbar(activeTab, new string[] { "Main settings", "Parts Collection" });

            switch (activeTab)
            {
                case 0:
                    DrawTest();
                    break;
                case 1:
                    DrawPartsCollectionGUI();
                    break;

            }
        }

        private void OnEnable()
        {
            //get part collection from mapCreatorManager
        }

        private void DrawTest()
        {
            GUILayout.BeginVertical("box");
            foreach (Part part in Resources.FindObjectsOfTypeAll(typeof(Part)))
            {
                GUILayout.BeginHorizontal("box");
                GUILayout.Label(part.gameObject.name);
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }

        private void DrawPartsCollectionGUI()
        {
            //serializedObject.Update();

            GUILayout.Space(10);

            #region Inspector

            GUILayout.BeginVertical("box");

            #region Parts Collection Settings

            GUI.color = Color.white;

            GUI.enabled = false;

            EditorGUILayout.ObjectField("Current parts collection:", MapCreatorManager.GetInstance().PartsCollection, typeof(PartsCollection), true);

            GUI.enabled = true;

            GUILayout.BeginVertical();

            if (PartsCollection.parts.Count == 0)
            {
                GUILayout.BeginHorizontal("box");

                GUILayout.Label("The list does not contain parts.");

                GUILayout.EndHorizontal();
            }
            else
            {
                GUILayout.BeginHorizontal();

                GUI.color = Color.white;//MainEditor.GetEditorColor;

                // if (GUILayout.Button("Sort Alphabetically"))
                //     Target.Parts = Target.Parts.OrderBy(e => e.Name).ToList();

                // if (GUILayout.Button("Sort Numerically"))
                //     Target.Parts = Target.Parts.OrderBy(e => e.Id).ToList();

                // GUI.color = Color.white;

                GUILayout.EndHorizontal();

                ScrollPosition = EditorGUILayout.BeginScrollView(ScrollPosition, false, false, GUILayout.Height(150));

                foreach (Part part in PartsCollection.parts)
                {
                    if (part == null)
                    {
                        PartsCollection.parts.Remove(part);

                        EditorUtility.SetDirty(PartsCollection);

                        return;
                    }

                    GUILayout.BeginHorizontal("box");

                    GUILayout.BeginVertical();

                    GUILayout.Space(2);

                    GUILayout.Label("[ID:" + part.Id + "] " + Regex.Replace(part.Name.ToString(), "([a-z])([A-Z])", "$1 $2"));

                    GUI.color = Color.white;

                    GUILayout.Space(2);

                    GUILayout.EndVertical();

                    // if (GUILayout.Button("Up", GUILayout.Width(50)))
                    // {
                    //     try
                    //     {
                    //         ListExtension.Move<PartBehaviour>(Target.Parts, Target.Parts.IndexOf(part), ListExtension.MoveDirection.Up);
                    //     }
                    //     catch
                    //     {
                    //     }
                    // }

                    // if (GUILayout.Button("Down", GUILayout.Width(50)))
                    // {
                    //     try
                    //     {
                    //         ListExtension.Move<PartBehaviour>(Target.Parts, Target.Parts.IndexOf(part), ListExtension.MoveDirection.Down);
                    //     }
                    //     catch
                    //     {
                    //     }
                    // }

                    GUI.color = Color.white;//MainEditor.GetEditorColor;

                    if (GUILayout.Button("Configure Part", GUILayout.Width(100)))
                    {
                        Selection.activeGameObject = part.gameObject;
                    }

                    GUI.color = new Color(1.5f, 0, 0);

                    if (GUILayout.Button("Remove", GUILayout.Width(80)))
                    {
                        PartsCollection.parts.Remove(part);

                        EditorUtility.SetDirty(PartsCollection);

                        return;
                    }

                    GUI.color = Color.white;

                    GUILayout.EndHorizontal();
                }

                GUILayout.Space(100);

                EditorGUILayout.EndScrollView();
            }

            GUI.color = Color.white;

            try
            {
                GUILayout.BeginVertical("box");

                GUILayout.BeginVertical();

                GUILayout.BeginHorizontal();

                prefabToAdd = (Part)EditorGUILayout.ObjectField("New part :", prefabToAdd, typeof(Part), false);

                GUILayout.EndHorizontal();

                GUI.enabled = prefabToAdd != null;

                GUI.color = Color.yellow;//MainEditor.GetEditorColor;

                if (GUILayout.Button("Add part to list"))
                {
                    if (prefabToAdd == null)
                    {
                        logChannel.LogError("Empty field. Put a part to \"New part\" field");
                        return;
                    }

                    if (!PartsCollection.parts.Contains(prefabToAdd))
                    {
                        Undo.RecordObject(PartsCollection, "Modified parts collection");

                        PartsCollection.parts.Add(prefabToAdd);

                        prefabToAdd = null;

                        Repaint();
                    }
                    else
                        logChannel.LogError("This part already exists in the collection.");
                }

                GUI.enabled = false;

                if (GUILayout.Button("Clear All Part(s) List"))
                {
                    // if (EditorUtility.DisplayDialog("Easy Build System - Information", "Do you want remove all the part(s) from the collection ?", "Ok", "Cancel"))
                    // {
                    //     Target.Parts.Clear();

                    //     Debug.Log("<b><color=cyan>[Easy Build System]</color></b> : The collection has been clear !.");
                    // }
                }

                GUI.enabled = true;

                GUI.color = Color.white;

                GUILayout.EndVertical();

                GUILayout.EndVertical();
            }
            catch { }

            GUILayout.Space(3);

            GUILayout.EndVertical();

            #endregion Parts Collection Settings

            GUILayout.EndVertical();

            #endregion Inspector

            //serializedObject.ApplyModifiedProperties();

            GUILayout.Space(10);
        }
    }
}
