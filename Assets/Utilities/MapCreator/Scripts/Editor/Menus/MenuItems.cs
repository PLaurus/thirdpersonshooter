﻿using System.Collections;
using System.Collections.Generic;
using MapCreatorUtility.Editor.Windows;
using MapCreatorUtility.Runtimes.MainBuilder;
using UnityEditor;
using UnityEngine;

namespace MapCreatorUtility.Editor.Menus
{
    public static class MenuItems
    {

        [MenuItem("MapCreator/Open Map Creator", false, 0)]
        public static void OpenMapCreator()
        {
            MapCreator.Start();
        }

        [MenuItem("MapCreator/Settings", false, 50)]
        public static void OpenSettingsWindow()
        {
            MapCreatorWindow.OpenWindow();
        }

        [MenuItem("MapCreator/Settings", true, 50)]
        [MenuItem("MapCreator/Close Map Creator", true, 100)]
        public static bool EnableSettings()
        {
            return MapCreator.IsEnabled();
        }

        [MenuItem("MapCreator/Close Map Creator", false, 100)]
        public static void CloseMapCreator()
        {
            MapCreator.Close();
        }
        
    }
}
