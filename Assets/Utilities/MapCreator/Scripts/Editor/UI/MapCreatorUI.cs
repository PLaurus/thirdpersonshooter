﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using Helpers;
using MapCreatorUtility.Runtimes.MainBuilder;
using MapCreatorUtility.Runtimes.Events;

namespace MapCreatorUtility.Editor.UI{

	public class MapCreatorUI {

		private static class Styles
		{
			private static bool isInitialized = false;

			public static GUIStyle backgroundStyle = new GUIStyle ();
            public static GUIStyle gridButtonStyle = new GUIStyle();

            //Mode toolbar styles
            public static ToggleContent noneModeContent = new ToggleContent("None","None","Disables any modelling modes");
            public static ToggleContent placementModeContent = new ToggleContent("Placement", "Placement", "Toggles placement mode on or off");
            public static ToggleContent destructionModeContent = new ToggleContent("Destruction", "Placement", "Toggles destruction mode on or off");
            public static ToggleContent editionModeContent = new ToggleContent("Edition", "Placement", "Toggles edition mode on or off");

            //Placement toolbar styles
            public static ToggleContent wallPlacement = new ToggleContent("Wall", "Wall", "Place a wall");
            public static ToggleContent floorPlacement = new ToggleContent("Floor", "Floor", "Place a floor");

            //public static GUIContent extendMenuContent = new GUIContent("", "Show or hide the scene view menu.");

            public static Color modeToolbarBackgroundColor = new Color(0f, 0f, 0f, .5f);
            public static Color placemntToolbarBackgroundColor = new Color(0f, 0f, 0f, .5f);

            public static int backgroundPadding = 4;

            public static void Initialize(){
				if (isInitialized)
					return;

                noneModeContent.imageOn = IconUtility.LoadIcon("NoneOn.png");
                noneModeContent.imageOff = IconUtility.LoadIcon("NoneOff.png");
                placementModeContent.imageOn = IconUtility.LoadIcon("PlacementOn.png");
                placementModeContent.imageOff = IconUtility.LoadIcon("PlacementOff.png");
                destructionModeContent.imageOn = IconUtility.LoadIcon("DestructionOn.png");
                destructionModeContent.imageOff = IconUtility.LoadIcon("DestructionOff.png");
                editionModeContent.imageOn = IconUtility.LoadIcon("EditionOn.png");
                editionModeContent.imageOff = IconUtility.LoadIcon("EditionOff.png");

                backgroundStyle.normal.background = EditorGUIUtility.whiteTexture;

				isInitialized = true;
			}
		}

        private static MapCreatorUI singleton;
        private static readonly Vector2Int modeToolbarPosition = new Vector2Int(108, Styles.backgroundPadding);
        private static readonly Vector2Int placementToolbarPosition = new Vector2Int(Styles.backgroundPadding, 25);
        private static LogChannel log = new LogChannel("MapCreatorUI");

        private bool initialized = false;

        private MapCreator mapCreator;

        private const int item_padding = 3;

        private bool extendoButtonHovering = false;
        private bool mouseOverModeToolbar = false;
        private bool mouseOverPlacementToolbar = false;

        private Rect modeButtonRect = new Rect(modeToolbarPosition.x, modeToolbarPosition.y, 0, 16);
        private Rect placementButtonRect = new Rect(placementToolbarPosition.x, placementToolbarPosition.y, 16 * 3, 16 * 2   );
        //private static Rect extendoButtonRect = new Rect(0, 0, 0, 0);
        private Rect backgroundModeToolbarRect = new Rect(0, 0, 0, 0);
        private Rect backgroundPlacementToolbarRect = new Rect(0, 0, 0, 0);

        private float deltaTime = 0f;
        private float lastTime = 0f;

        private const float fadeSpeed = 2.5f;

        private MapCreatorUI()
        {
        }

        public static MapCreatorUI GetInstance()
        {
            if (singleton == null)
                singleton = new MapCreatorUI();

            return singleton;
        }

        public void Initialize(MapCreator mapCreator)
        {
            this.mapCreator = mapCreator;
            Styles.Initialize();

            initialized = true;
            
            Show();

            
        }

        public void Uninitialize(){
            Hide();
            
            this.mapCreator = null;

            initialized = false;
        }

        private void Show()
        {
            if (initialized)
            {
                Hide();

                EditorApplication.update += SceneViewUpdate;
                SceneView.onSceneGUIDelegate += DrawSceneToolbars;
            }
            else
            {
                log.LogError("Map creator is not initialized");
            }
        }

        private void Hide()
        {
            EditorApplication.update -= SceneViewUpdate;
            SceneView.onSceneGUIDelegate -= DrawSceneToolbars;
        }

        #region Private Methods

        private void SceneViewUpdate()
        {
            deltaTime = Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;

            float modeAlpha = Styles.modeToolbarBackgroundColor.a;
            float placementAlpha = Styles.placemntToolbarBackgroundColor.a;

            Styles.modeToolbarBackgroundColor.a = Mathf.Clamp(Styles.modeToolbarBackgroundColor.a + (mouseOverModeToolbar ? fadeSpeed : -fadeSpeed) * deltaTime, 0f, .5f);
            Styles.placemntToolbarBackgroundColor.a = Mathf.Clamp(Styles.placemntToolbarBackgroundColor.a + (mouseOverPlacementToolbar ? fadeSpeed : -fadeSpeed) * deltaTime, 0f, .5f);
            //extendoNormalColor.a = menuBackgroundColor.a;
            //extendoHoverColor.a = (menuBackgroundColor.a / .5f);

            if (!Mathf.Approximately(Styles.modeToolbarBackgroundColor.a, modeAlpha) || !Mathf.Approximately(Styles.placemntToolbarBackgroundColor.a, placementAlpha))
                SceneView.RepaintAll();
        }

		private void DrawSceneToolbars(SceneView sceneView)
        {
            Event currentEvent = Event.current;

            if (sceneView == SceneView.lastActiveSceneView)
            {
                Handles.BeginGUI();

                DrawModeToolbar(currentEvent);
                // if (mapCreator.activeBuildMode == BuildMode.Placement)
                //     DrawPlacementToolbar(currentEvent);

                Handles.EndGUI();
            }
        }

        private void DrawModeToolbar(Event currentEvent)
        {
            // repaint scene gui if mouse is near controls
            if (currentEvent.type == EventType.MouseMove)
            {
                /*bool tmp = extendoButtonHovering;
                extendoButtonHovering = extendoButtonRect.Contains(currentEvent.mousePosition);

                if (extendoButtonHovering != tmp)
                    SceneView.RepaintAll();*/

                mouseOverModeToolbar = backgroundModeToolbarRect.Contains(currentEvent.mousePosition);
            }

            GUI.backgroundColor = Styles.modeToolbarBackgroundColor;
            backgroundModeToolbarRect.x = modeToolbarPosition.x - Styles.backgroundPadding;
            backgroundModeToolbarRect.y = 0;
            backgroundModeToolbarRect.width = modeButtonRect.x - modeToolbarPosition.x + modeButtonRect.width + 2 * Styles.backgroundPadding;
            backgroundModeToolbarRect.height = modeButtonRect.y + modeButtonRect.height + Styles.backgroundPadding;
            GUI.Box(backgroundModeToolbarRect, "", Styles.backgroundStyle);

            // when hit testing mouse for showing the background, add some leeway
            backgroundModeToolbarRect.x -= 32f;
            backgroundModeToolbarRect.width += 32f * 2;
            backgroundModeToolbarRect.height += 32f;
            GUI.backgroundColor = Color.white;

            modeButtonRect.x = modeToolbarPosition.x;
            modeButtonRect.width = Styles.noneModeContent.imageOn.width * modeButtonRect.height / Styles.noneModeContent.imageOn.height;

            if (ToggleContent.ToggleButton(
                modeButtonRect,
                Styles.noneModeContent,
                mapCreator.activeBuildMode == BuildMode.None,
                Styles.gridButtonStyle,
                EditorStyles.miniButton
                ))
                mapCreator.ChangeMode(BuildMode.None);

            modeButtonRect.x += modeButtonRect.width + item_padding;
            modeButtonRect.width = Styles.placementModeContent.imageOn.width * modeButtonRect.height / Styles.placementModeContent.imageOn.height;

            if (ToggleContent.ToggleButton(
                modeButtonRect,
                Styles.placementModeContent,
                mapCreator.activeBuildMode == BuildMode.Placement,
                Styles.gridButtonStyle,
                EditorStyles.miniButton
                ))
                mapCreator.ChangeMode(BuildMode.Placement);

            modeButtonRect.x += modeButtonRect.width + item_padding;
            modeButtonRect.width = Styles.destructionModeContent.imageOn.width * modeButtonRect.height / Styles.destructionModeContent.imageOn.height;

            if (ToggleContent.ToggleButton(
                modeButtonRect,
                Styles.destructionModeContent,
                mapCreator.activeBuildMode == BuildMode.Destruction,
                Styles.gridButtonStyle,
                EditorStyles.miniButton
                ))
                mapCreator.ChangeMode(BuildMode.Destruction);

            modeButtonRect.x += modeButtonRect.width + item_padding;
            modeButtonRect.width = Styles.editionModeContent.imageOn.width * modeButtonRect.height / Styles.editionModeContent.imageOn.height;

            if (ToggleContent.ToggleButton(
                modeButtonRect,
                Styles.editionModeContent,
                mapCreator.activeBuildMode == BuildMode.Edition,
                Styles.gridButtonStyle,
                Styles.gridButtonStyle
                ))
                mapCreator.ChangeMode(BuildMode.Edition);
        }

        // private void DrawPlacementToolbar(Event currentEvent)
        // {
        //     // repaint scene gui if mouse is near controls
        //     if (currentEvent.type == EventType.MouseMove)
        //     {
        //         /*bool tmp = extendoButtonHovering;
        //         extendoButtonHovering = extendoButtonRect.Contains(currentEvent.mousePosition);

        //         if (extendoButtonHovering != tmp)
        //             SceneView.RepaintAll();*/

        //         mouseOverPlacementToolbar = backgroundPlacementToolbarRect.Contains(currentEvent.mousePosition);
        //     }

        //     GUI.backgroundColor = Styles.placemntToolbarBackgroundColor;
        //     backgroundPlacementToolbarRect.x = 0;
        //     backgroundPlacementToolbarRect.y = placementToolbarPosition.y - Styles.backgroundPadding;
        //     backgroundPlacementToolbarRect.width = placementButtonRect.x + placementButtonRect.width + Styles.backgroundPadding;
        //     backgroundPlacementToolbarRect.height = placementButtonRect.y - placementToolbarPosition.y + placementButtonRect.height + 2 * Styles.backgroundPadding;//modeButtonRect.y + modeButtonRect.height + Styles.backgroundPadding;
        //     GUI.Box(backgroundPlacementToolbarRect, "", Styles.backgroundStyle);

        //     // when hit testing mouse for showing the background, add some leeway
        //     backgroundPlacementToolbarRect.y -= 32f;
        //     backgroundPlacementToolbarRect.width += 32f;
        //     backgroundPlacementToolbarRect.height += 32f * 2;
        //     GUI.backgroundColor = Color.white;

        //     placementButtonRect.y = placementToolbarPosition.y;
        //     //placementButtonRect.width = Styles.noneModeContent.imageOn.width * placementButtonRect.height / Styles.noneModeContent.imageOn.height;

        //     if (ToggleContent.ToggleButton(
        //         placementButtonRect,
        //         Styles.wallPlacement,
        //         mapCreator.activePlacementObject == PlacementObject.Wall,
        //         Styles.gridButtonStyle,
        //         EditorStyles.miniButton
        //         ))
        //         mapCreator.SetActivePlacementObject(PlacementObject.Wall);

        //     placementButtonRect.y += placementButtonRect.height + item_padding;
        //     //modeButtonRect.width = Styles.placementModeContent.imageOn.width * modeButtonRect.height / Styles.placementModeContent.imageOn.height;

        //     if (ToggleContent.ToggleButton(
        //        placementButtonRect,
        //        Styles.floorPlacement,
        //        mapCreator.activePlacementObject == PlacementObject.Wall,
        //        Styles.gridButtonStyle,
        //        EditorStyles.miniButton
        //        ))
        //        mapCreator.SetActivePlacementObject(PlacementObject.Floor);

        // }

        #endregion
    }
}
