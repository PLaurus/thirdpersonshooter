﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapCreatorUtility.Editor.UI{
	/*
	 * Substitute for GUIContent that offers some additional functionality
	*/
	[System.Serializable]
	public class ToggleContent {
		public string textOn, textOff;
		public Texture2D imageOn, imageOff;
		public string tooltip;

		GUIContent guiContent = new GUIContent();

		public ToggleContent(string textOn, string textOff, string tooltip){
			this.textOn = textOn;
			this.textOff = textOff;
			this.imageOn = (Texture2D)null;
			this.imageOff = (Texture2D)null;
			this.tooltip = tooltip;

			guiContent.tooltip = tooltip;
		}

		public ToggleContent(string textOn, string textOff, Texture2D imageOn, Texture2D imageOff, string tooltip){
			this.textOn = textOn;
			this.textOff = textOff;
			this.imageOn = imageOn;
			this.imageOff = imageOff;
			this.tooltip = tooltip;

			guiContent.tooltip = tooltip;
		}

        public static bool ToggleButton(Rect rectangle, ToggleContent toggleContent, bool enabled, GUIStyle imageStyle, GUIStyle alternativeStyle)
        {
            toggleContent.guiContent.image = enabled ? toggleContent.imageOn : toggleContent.imageOff;
            toggleContent.guiContent.text = toggleContent.guiContent.image == null ? (enabled ? toggleContent.textOn : toggleContent.textOff) : "";

            return GUI.Button(rectangle, toggleContent.guiContent, toggleContent.guiContent.image != null ? imageStyle : alternativeStyle);
        }



	}
}
