﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace MapCreatorUtility.Editor.UI
{

    [InitializeOnLoad]
    public static class IconUtility
    {
        private const string ICONS_FOLDER_NAME = "MapCreatorIcons";
        private static string iconsFolderPath = "Assets/Utilites/MapCreator/MapCreatorIcons/";

        static IconUtility(){
            if (!Directory.Exists(iconsFolderPath))
            {
                string folder = FindFolder(ICONS_FOLDER_NAME);

                if (Directory.Exists(folder))
                    iconsFolderPath = folder;
            }
        }

        private static string FindFolder(string folderName)
        {
            string name = folderName.Replace("\\", "/").Substring(folderName.LastIndexOf('/') + 1);
            string[] matches = Directory.GetDirectories("Assets/", name, SearchOption.AllDirectories);

            foreach(string match in matches)
            {
                string path = match.Replace("\\", "/");

                if (path.Contains(name))
                {
                    if (!path.EndsWith("/"))
                        path += "/";

                    return path;
                }
            }

            Debug.LogError("Could not locate " + folderName + " folder.");

            return "";
        }

        public static Texture2D LoadIcon(string iconName)
        {
            string iconPath = string.Format("{0}{1}", iconsFolderPath, iconName);

            if (!File.Exists(iconPath))
            {
                Debug.LogError("Failed to locate menu icon: " + iconName + ".\nThis can happen if the icons folder (" + ICONS_FOLDER_NAME + ") is moved or deleted.");
                return (Texture2D)null;
            }

            return LoadAssetAtPath<Texture2D>(iconPath);
        }
    
        static T LoadAssetAtPath<T>(string path) where T : Object
        {
            return (T)AssetDatabase.LoadAssetAtPath(path, typeof(T));
        }
    }
}
#endif
