﻿using MapCreatorUtility.Helpers;
using MapCreatorUtility.Runtimes.Events;
using MapCreatorUtility.Runtimes.MainBuilder;
using MapCreatorUtility.Runtimes.Parts;
using MapCreatorUtility.Runtimes.Parts.Data;
using MapCreatorUtility.Runtimes.Sockets;
using MapCreatorUtility.Values;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LogChannel = MapCreatorUtility.Helpers.LogChannel;

namespace MapCreatorUtility.Runtimes.Managers
{
    public enum SupportType
    {
        All,
        Terrain,
        Voxeland,
        Surface
    }

    public class MapCreatorManager
    {
        #region Private Fields

        private static MapCreatorManager instance;

        private LogChannel logChannel = new LogChannel("MapCreatorManager");

        #endregion

        #region Public Fields

        public SupportType BuildingSupport;

        public bool UsePhysics = true;

        private PartsCollection partsCollection;

        public PartsCollection PartsCollection{
            get{
                if(partsCollection != null){
                    return partsCollection;
                }

                partsCollection = ScriptableObjectHelper.LoadAsset<PartsCollection>(Constants.PARTS_COLLECTION_RESOURCES_PATH, Constants.PARTS_COLLECTION_NAME);

                if(partsCollection == null){
                    logChannel.LogError("Failed to find parts collection");
                    partsCollection = ScriptableObjectHelper.CreateAsset<PartsCollection>(Constants.PARTS_COLLECTION_PATH, Constants.PARTS_COLLECTION_NAME);
                    logChannel.LogInformation("Created new parts collection");
                }

                return partsCollection;
            }

            set{
                partsCollection = value;
            }
        }

        public bool useDefaultPreviewMaterial = false;
        public Material customPreviewMaterial;

        public Color PreviewAllowedColor = new Color(0, 1.0f, 0, 0.5f);

        public Color PreviewDeniedColor = new Color(1.0f, 0, 0, 0.5f);

        [HideInInspector]
        public List<Part> parts = new List<Part>();

        [HideInInspector]
        public List<PartSocket> sockets = new List<PartSocket>();

        // [HideInInspector]
        // public List<AreaBehaviour> Areas = new List<AreaBehaviour>();

        [HideInInspector]
        public PartState defaultPartState = PartState.Placed;

        #endregion

        #region Private Methods

        private MapCreatorManager()
        {
            //восстанавливаем значения переменных, сохраненных в EditorPreferences
        }

        // private void Start()
        // {
        //     if (BuildingSupport == SupportType.Terrain)
        //     {
        //         if (FindObjectOfType<UnityEngine.Terrain>() != null)
        //             TerrainManager.Initialize();
        //     }
        //     else if (FindObjectOfType<UnityEngine.Terrain>() != null)
        //     {
        //         Debug.LogWarning("<b><color=yellow>[Easy Build System]</color></b> : A terrain was detected but the Build Manager did not consider the Placement Support Type.");
        //     }
        // }

        #endregion

        #region Public Methods

        public static MapCreatorManager GetInstance()
        {
            if(!MapCreator.IsEnabled())
                return null;
                
            if (instance == null)
                instance = new MapCreatorManager();

            return instance;
        }

        /// <summary>
        /// This method allows to add a part from the manager cache.
        /// </summary>
        public void AddPart(Part part)
        {
            if (part == null)
                return;

            parts.Add(part);
        }

        /// <summary>
        /// This method allows to remove a part from the manager cache.
        /// </summary>
        public void RemovePart(Part part)
        {
            if (part == null)
                return;

            parts.Remove(part);
        }

        /// <summary>
        /// This method allows to add a socket from the manager cache.
        /// </summary>
        public void AddSocket(PartSocket socket)
        {
            if (socket == null)
                return;

            sockets.Add(socket);
        }

        /// <summary>
        /// This method allows to remove a socket from the manager cache.
        /// </summary>
        public void RemoveSocket(PartSocket socket)
        {
            if (socket == null)
                return;

            sockets.Remove(socket);
        }

        // /// <summary>
        // /// This method allows to add a area from the manager cache.
        // /// </summary>
        // public void AddArea(AreaBehaviour area)
        // {
        //     if (area == null)
        //         return;

        //     Areas.Add(area);
        // }

        // /// <summary>
        // /// This method allows to remove a area from the manager cache.
        // /// </summary>
        // public void RemoveArea(AreaBehaviour area)
        // {
        //     if (area == null)
        //         return;

        //     Areas.Remove(area);
        // }

        /// <summary>
        /// This method allows to get a prefab by id.
        /// </summary>
        public Part GetPart(int id)
        {
            return PartsCollection.parts.Find(entry => entry.Id == id);
        }

        /// <summary>
        /// This method allows to get a prefab by name.
        /// </summary>
        public Part GetPart(string name)
        {
            return PartsCollection.parts.Find(entry => entry.Name == name);
        }

        /// <summary>
        /// This method allows to place a prefab.
        /// </summary>
        public Part PlacePrefab(Part part, Vector3 position, Vector3 rotation, Vector3 scale, Transform parent = null, PartSocket socket = null)
        {
            GameObject PlacedTemp = Object.Instantiate(part.gameObject, position, Quaternion.Euler(rotation));

            PlacedTemp.transform.localScale = scale;

            Part PlacedPart = PlacedTemp.GetComponent<Part>();

            if (parent == null)
            {
                // if (socket != null)
                // {
                //     if (socket.socketOwner.HasGroup)
                //     {
                //         PlacedTemp.transform.SetParent(socket.socketOwner.transform.parent, true);
                //     }
                // }
                // else
                // {
                //     GameObject Group = new GameObject("Group (" + PlacedPart.GetInstanceID() + ")", typeof(GroupBehaviour));

                //     PlacedTemp.transform.SetParent(Group.transform, true);
                // }
            }
            else
                PlacedTemp.transform.SetParent(parent, true);

            PlacedPart.EntityInstanceId = PlacedPart.GetInstanceID();

            MapCreatorEvents.PartPlaced(PlacedPart, socket);

            PlacedPart.ChangeState(defaultPartState);

            return PlacedPart;
        }

        // /// <summary>
        // /// This method allows to get the nearest area.
        // /// </summary>
        // public AreaBehaviour GetNearestArea(Vector3 position)
        // {
        //     foreach (AreaBehaviour Area in Areas)
        //     {
        //         if (Area != null)
        //             if (Area.gameObject.activeSelf == true)
        //                 if (Vector3.Distance(position, Area.transform.position) <= Area.Radius)
        //                     return Area;
        //     }

        //     return null;
        // }
        #endregion


    }
}
