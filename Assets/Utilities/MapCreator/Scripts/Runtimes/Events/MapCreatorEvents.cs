﻿using System.Collections;
using System.Collections.Generic;
using MapCreatorUtility.Runtimes.MainBuilder;
using MapCreatorUtility.Runtimes.Parts;
using MapCreatorUtility.Runtimes.Sockets;
using UnityEngine;

namespace MapCreatorUtility.Runtimes.Events
{
    public class MapCreatorEvents
    {
        #region Builder Events

        public delegate void MapCreatorEnabledEvent(MapCreator mapCreatorInstance);
        public static event MapCreatorEnabledEvent onMapCreatorEnabled;

        /// <summary>
        /// This method is called when map creator enables.
        /// </summary>
        public static void MapCreatorEnabled(MapCreator mapCreatorInstance)
        {
            if (onMapCreatorEnabled != null)
                onMapCreatorEnabled.Invoke(mapCreatorInstance);
        }

        public delegate void MapCreatorDisabledEvent();
        public static event MapCreatorDisabledEvent onMapCreatorDisabled;

        /// <summary>
        /// This method is called when map creator enables.
        /// </summary>
        public static void MapCreatorDisabled()
        {
            if (onMapCreatorDisabled != null)
                onMapCreatorDisabled.Invoke();
        }

        public delegate void ChangeBuildModeEvent(BuildMode mode);
        public static event ChangeBuildModeEvent onBuildModeChanged;
        
        /// <summary>
        /// This method is called when the build mode is changed.
        /// </summary>
        public static void BuildModeChanged(BuildMode mode)
        {
            if (onBuildModeChanged != null)
                onBuildModeChanged.Invoke(mode);
        }

        public delegate void ChangePartStateEvent(Part part, PartState state);
        public static event ChangePartStateEvent onChangePartState;

        /// <summary>
        /// This method is called when the part change state.
        /// </summary>
        public static void PartStateChanged(Part part, PartState state)
        {
            if (onChangePartState != null)
                onChangePartState.Invoke(part, state);
        }

        public delegate void PartPreviewCreatedEvent(Part part);
        public static event PartPreviewCreatedEvent onPartPreviewCreated;

        /// <summary>
        /// This method is called when the a preview is created.
        /// </summary>
        public static void PartPreviewCreated(Part part)
        {
            if (onPartPreviewCreated != null)
                onPartPreviewCreated.Invoke(part);
        }

        public delegate void PartPreviewCanceledEvent(Part part);
        public static event PartPreviewCanceledEvent onPartPreviewCanceled;

        /// <summary>
        /// This method is called when the a preview is canceled.
        /// </summary>
        public static void PartPreviewCanceled(Part part)
        {
            if (onPartPreviewCanceled != null)
                onPartPreviewCanceled.Invoke(part);
        }

        public delegate void PartPlacedEvent(Part part, PartSocket socket);
        public static event PartPlacedEvent onPartPlaced;

        /// <summary>
        /// This method is called when a part is placed.
        /// </summary>
        public static void PartPlaced(Part part, PartSocket socket)
        {
            if (onPartPlaced != null)
                onPartPlaced.Invoke(part, socket);
        }

        public delegate void ChangedPartAppearanceEvent(Part part, int appearanceIndex);
        public static event ChangedPartAppearanceEvent onChangedPartAppearance;

        /// <summary>
        /// This method is called when the part appearance change.
        /// </summary>
        public static void ChangedPartAppearance(Part part, int appearanceIndex)
        {
            if (onChangedPartAppearance != null)
                onChangedPartAppearance.Invoke(part, appearanceIndex);
        }

        public delegate void PartDestroyedEvent(Part part);
        public static event PartDestroyedEvent onDestroyedPart;

        /// <summary>
        /// This method is called when a part is removed.
        /// </summary>
        public static void PartDestroyed(Part part)
        {
            if (onDestroyedPart != null)
                onDestroyedPart.Invoke(part);
        }

        public delegate void PartEditedEvent(Part part, PartSocket socket);
        public static event PartEditedEvent onEditedPart;

        /// <summary>
        /// This method is called when a part is edited.
        /// </summary>
        public static void PartEdited(Part part, PartSocket socket)
        {
            if (onEditedPart != null)
                onEditedPart.Invoke(part, socket);
        }

        #endregion Builder Events

        /*#region Storage Events

        public delegate void EventHandlerStorageSaving();

        public static event EventHandlerStorageSaving OnStorageSaving;

        /// <summary>
        /// This method is called during the saving process of the current scene.
        /// </summary>
        public static void StorageSaving()
        {
            if (OnStorageSaving != null)
                OnStorageSaving.Invoke();
        }

        public delegate void EventHandlerStorageLoading();

        public static event EventHandlerStorageLoading OnStorageLoading;

        /// <summary>
        /// This method is called during the loading process of the current scene.
        /// </summary>
        public static void StorageLoading()
        {
            if (OnStorageLoading != null)
                OnStorageLoading.Invoke();
        }

        public delegate void EventHandlerStorageLoadingDone(PartBehaviour[] Parts);

        public static event EventHandlerStorageLoadingDone OnStorageLoadingDone;

        /// <summary>
        /// This method is called when the loading process is done.
        /// </summary>
        public static void StorageLoadingDone(PartBehaviour[] Parts)
        {
            if (OnStorageLoadingDone != null)
                OnStorageLoadingDone.Invoke(Parts);
        }

        public delegate void EventHandlerStorageSavingDone(PartBehaviour[] Parts);

        public static event EventHandlerStorageSavingDone OnStorageSavingDone;

        /// <summary>
        /// This method is called when the saving process is done.
        /// </summary>
        public static void StorageSavingDone(PartBehaviour[] Parts)
        {
            if (OnStorageSavingDone != null)
                OnStorageSavingDone.Invoke(Parts);
        }

        public delegate void EventHandlerStorageFailed(string exception);

        public static event EventHandlerStorageFailed OnStorageFailed;

        /// <summary>
        /// This method is called when the loading or saving process has failed.
        /// </summary>
        public static void StorageFailed(string exception)
        {
            if (OnStorageFailed != null)
                OnStorageFailed.Invoke(exception);
        }

        public delegate void EventHandlerStorageDeleted();

        public static event EventHandlerStorageDeleted OnStorageDeleted;

        /// <summary>
        /// This method is called when the file is deleted.
        /// </summary>
        public static void StorageDeleted()
        {
            if (OnStorageDeleted != null)
                OnStorageDeleted.Invoke();
        }

        #endregion Storage Events*/
    }
}
