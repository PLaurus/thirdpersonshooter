﻿using System.Collections;
using System.Collections.Generic;
using MapCreatorUtility.Runtimes.Parts;
using UnityEngine;

namespace MapCreatorUtility.Runtimes.Parts.Data
{
    public class PartsCollection : ScriptableObject
    {
        #region Public Fields

        public List<Part> parts = new List<Part>();

        #endregion Public Fields

        #region Public Methods

        /// <summary>
        /// This method allows to check if the collection does not contains the same part Id.
        /// </summary>
        public bool CheckPartsCollection()
        {
            List<Part> CacheParts = new List<Part>();

            foreach (Part Part in parts)
                if (Part != null)
                    CacheParts.Add(Part);

            foreach (Part CachePart in CacheParts)
            {
                foreach (Part Part in parts)
                {
                    if (Part != null)
                    {
                        if (CachePart.Name != Part.Name)
                        {
                            if (CachePart.Id == Part.Id)
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        #endregion Public Methods
    }
}
