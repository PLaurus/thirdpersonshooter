﻿using MapCreatorUtility.Helpers;
using MapCreatorUtility.Helpers.Extensions;
using MapCreatorUtility.Runtimes.Parts.Data;
using MapCreatorUtility.Runtimes.Sockets;
using MapCreatorUtility.Runtimes.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MapCreatorUtility.Values;

namespace MapCreatorUtility.Runtimes.Parts
{
    public enum PartType
    {
        None = 0,
        Foundation = 1,
        Pillar = 2,
        Wall = 3,
        Floor = 4,
        Stair = 5,
        Door = 6,
        Window = 7
    }

    public enum PartState
    {
        Queue,//TODO: используется?
        Preview,
        Remove,
        Edit,
        Placed
    }

    public enum SurfaceType
    {
        SurfaceAndTerrain = 0,
        Foundation = 1,
        Pillar = 2,
        Wall = 3,
        Floor = 4,
        Stair = 5,
        Door = 6,
        Window = 7
    }

    [ExecuteInEditMode]
    public class Part : MonoBehaviour
    {
        #region Public Fields

        #region Base Settings

        public bool advancedFeatures;

        public int Id;

        public string Name = "New Part";

        public PartType Type = PartType.Foundation;

        public Part[] OccupancyParts;

        public bool FreePlacement = false;

        public bool AvoidClipping = true;

        public bool AvoidClippingOnSocket = false;

        public bool AvoidAnchoredOnSocket = false;

        public bool RequireSockets;

        #endregion Base Settings

        #region Preview Settings

        public bool UseGroundUpper;

        public float GroundUpperHeight = 1f;

        public bool RotateOnSockets = false;

        public bool RotateAccordingSlope;

        public Vector3 RotationAxis = Vector3.up * 90;

        public Vector3 PreviewOffset = new Vector3(0, 0.03f, 0);

        #region Elements to disable on preview

        public GameObject[] objectsToDisableOnPreview;

        public MonoBehaviour[] behavioursToDisableOnPreview;

        public Collider[] collidersToDisableOnPreview;

        #endregion

        public bool PreviewUseColorLerpTime = false;

        public float PreviewColorLerpTime = 15.0f;

        public Material previewMaterial;

        #endregion Preview Settings

        #region Appearances Settings

        //public bool useAppearances;

        //public List<GameObject> Appearances = new List<GameObject>();

        //public int AppearanceIndex = 0;

        #endregion Appearances Settings

        #region Meshs Settings

        public Bounds meshBounds;

        #endregion Meshs Settings

        #region Physics Settings

        public bool useConditionalPhysics;//done

        public LayerMask PhysicsLayers;

        public float PhysicsLifeTime = 5f;

        public bool PhysicsConvexOnAffected = true;

        public bool PhysicsOnlyStablePlacement;

        public string[] PhysicsIgnoreTags;

        public Detection[] CustomDetections;

        #endregion Physics Settings

        #region Terrain Settings

        public bool UseTerrainPrevention;

        public Bounds TerrainBounds;

        #endregion Terrain Settings

        [HideInInspector]
        public int EntityInstanceId;

        [HideInInspector]//done
        public PartState currentState = PartState.Placed;//done

        [HideInInspector]
        public PartState LastState;

        // [HideInInspector]
        // public bool HasGroup { get { return (GetComponentInParent<GroupBehaviour>() != null); } }

        [HideInInspector]
        public bool AffectedByPhysics;

        [HideInInspector]//done
        public List<Part> linkedParts;//done

        [HideInInspector]//done
        public Dictionary<Renderer, Material[]> initialRenders = new Dictionary<Renderer, Material[]>();//done

        [HideInInspector]//done
        public List<Collider> partColliders = new List<Collider>();//done

        [HideInInspector]//done
        public List<Renderer> renderers = new List<Renderer>();//done

        [HideInInspector]//done
        public PartSocket[] sockets;//done

        #endregion Public Fields

        #region Private Fields

        private bool Quitting;

        private int InitAppearanceIndex;

        #endregion Private Fields

        #region Private Methods

        private void OnEnable()
        {
            MapCreatorEvents.onDestroyedPart += OnPartDestroyed;
        }

        private void OnDisable()
        {
            MapCreatorEvents.onDestroyedPart -= OnPartDestroyed;
        }

        // private void Awake()//done
        // {
        //     //InitAppearanceIndex = AppearanceIndex;

        //     if (MapCreatorManager.GetInstance() != null)//done
        //     {
        //         if (MapCreatorManager.GetInstance().useDefaultPreviewMaterial)
        //         {
        //             previewMaterial = new Material(MapCreatorManager.GetInstance().customPreviewMaterial);
        //         }
        //         else
        //         {
        //             previewMaterial = new Material(Shader.Find(Constants.TRANSPARENT_SHADER_NAME))
        //             {
        //                 color = new Color(0f, 0f, 0f, 0f)//TODO: работает?
        //             };
        //         }
        //     }
        //     else
        //     {
        //         previewMaterial = new Material(Shader.Find(Constants.TRANSPARENT_SHADER_NAME))
        //         {
        //             color = new Color(0f, 0f, 0f, 0f)
        //         };
        //     }

        //     renderers = GetComponentsInChildren<Renderer>(true).ToList();//done

        //     for (int i = 0; i < renderers.Count; i++)//done
        //         initialRenders.Add(renderers[i], renderers[i].sharedMaterials);//done

        //     partColliders = GetComponentsInChildren<Collider>(true).ToList();//done

        //     /*for (int i = 0; i < partColliders.Count; i++)
        //         if (partColliders[i] != partColliders[i])
        //             Physics.IgnoreCollision(partColliders[i], partColliders[i]);*///это хуета какая то

        //     sockets = GetComponentsInChildren<PartSocket>();

        //     if (!advancedFeatures)
        //         meshBounds = gameObject.GetChildsBounds();
        // }

        //move to update with init condition (MapCreatorLaunchedEvent) or if(entered edit mode first time)
        // private void Start()
        // {
        //     if (currentState != PartState.Preview)
        //     {
        //         MapCreatorManager.GetInstance().AddPart(this);//done

        //         if (useConditionalPhysics)
        //             gameObject.AddRigibody(true, true);

        //         ChangeAreaState(OccupancyType.Busy);

        //         if (!CheckStability())
        //             ApplyPhysics();
        //     }
        // }

        private void Update()
        {
            bool isNotPlaced = currentState != PartState.Placed;//done

            if (isNotPlaced)
            {
                EnableElementsDisabledOnPreview(false);//done
                return;//done
            }

            /*if (useAppearances)
            {
                for (int i = 0; i < Appearances.Count; i++)
                {
                    if (Appearances[i] == null)
                        return;

                    if (i == AppearanceIndex)
                        Appearances[i].SetActive(true);
                    else
                        Appearances[i].SetActive(false);
                }
            }*/
        }

        // private void OnDestroy()
        // {
        //     if (Quitting)
        //         return;

        //     if (currentState == PartState.Preview)
        //         return;

        //     MapCreatorEvents.PartDestroyed(GetComponent<Part>());

        //     MapCreatorManager.GetInstance().RemovePart(this);

        //     if (sockets.Length != 0)
        //     {
        //         for (int i = 0; i < sockets.Length; i++)
        //         {
        //             sockets[i].ChangeAreaState(OccupancyType.Free,
        //                 LayerMask.NameToLayer(Constants.LAYER_DEFAULT.ToLower()),
        //                 LayerMask.NameToLayer(Constants.LAYER_SOCKET.ToLower()), this);
        //         }
        //     }
        //     else
        //     {
        //         Collider[] Colliders = Physics.OverlapBox(GetWorldPartMeshBounds().center,
        //             GetWorldPartMeshBounds().extents,
        //             transform.rotation, LayerMask.NameToLayer(Constants.LAYER_SOCKET.ToLower()), QueryTriggerInteraction.Collide);

        //         for (int i = 0; i < Colliders.Length; i++)
        //         {
        //             PartSocket Socket = Colliders[i].GetComponent<PartSocket>();

        //             if (Socket != null)
        //                 Socket.ChangeState(OccupancyType.Free, this);
        //         }
        //     }
        // }

        private void OnPartDestroyed(Part part)
        {
            if (useConditionalPhysics)
            {
                if (!CheckStability())
                    ApplyPhysics();

                for (int i = 0; i < linkedParts.Count; i++)
                    if (linkedParts[i] != null)
                        if (!linkedParts[i].CheckStability())
                            linkedParts[i].ApplyPhysics();
            }
        }

        private void OnApplicationQuit()
        {
            Quitting = true;

            //AppearanceIndex = InitAppearanceIndex;
        }

        private void EnableElementsDisabledOnPreview(bool enabled)//done
        {
            foreach (GameObject obj in objectsToDisableOnPreview)
                if (obj)
                    obj.SetActive(enabled);

            foreach (MonoBehaviour behaviour in behavioursToDisableOnPreview)
                if (behaviour)
                    behaviour.enabled = enabled;

            foreach (Collider collider in collidersToDisableOnPreview)
                if (collider)
                    collider.enabled = enabled;
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// This method allows to change the part state (Queue, Preview, Edit, Remove, Placed).
        /// Queue = Preview Allowed Color, Disable Colliders, Enable Sockets.
        /// Preview = Preview Allowed Color, Disable Colliders, Disable Sockets.
        /// Edit = Preview Allowed Color, Disable Colliders, Enable Sockets.
        /// Remove = Preview Allowed Color, Disable Colliders, Enable Sockets.
        /// Placed = Preview Allowed Color, Disable Colliders, Enable Sockets.
        /// </summary>
        public void ChangeState(PartState state)
        {
            // if (MapCreator.GetInstance() == null)
            //     return;

            // if (currentState == state)
            //     return;

            // LastState = currentState;

            // if (state == PartState.Queue)
            // {
            //     gameObject.ChangeAllMaterialsInChildren(renderers.ToArray(), previewMaterial);

            //     gameObject.ChangeAllMaterialsColorInChildren(renderers.ToArray(), Color.clear);

            //     foreach (GameObject Obj in objectsToDisableOnPreview)
            //         if (Obj)
            //             Obj.SetActive(false);

            //     foreach (MonoBehaviour Behaviour in behavioursToDisableOnPreview)
            //         if (Behaviour)
            //             Behaviour.enabled = false;

            //     EnableAllColliders();

            //     foreach (Collider Collider in collidersToDisableOnPreview)
            //         if (Collider)
            //             Collider.enabled = false;

            //     foreach (PartSocket Socket in sockets)
            //     {
            //         Socket.EnableCollider();

            //         Socket.gameObject.SetActive(true);
            //     }
            // }
            // else if (state == PartState.Preview)
            // {
            //     gameObject.ChangeAllMaterialsInChildren(renderers.ToArray(), previewMaterial);

            //     gameObject.ChangeAllMaterialsColorInChildren(renderers.ToArray(),
            //         MapCreator.GetInstance().AllowPlacement ? MapCreatorManager.GetInstance().PreviewAllowedColor : MapCreatorManager.GetInstance().PreviewDeniedColor);

            //     foreach (GameObject Obj in objectsToDisableOnPreview)
            //         if (Obj)
            //             Obj.SetActive(false);

            //     foreach (MonoBehaviour Behaviour in behavioursToDisableOnPreview)
            //         if (Behaviour)
            //             Behaviour.enabled = false;

            //     DisableAllColliders();

            //     foreach (Collider Collider in collidersToDisableOnPreview)
            //         if (Collider)
            //             Collider.enabled = false;

            //     foreach (PartSocket Socket in sockets)
            //     {
            //         Socket.DisableCollider();

            //         Socket.gameObject.SetActive(false);
            //     }
            // }
            // else if (state == PartState.Edit)
            // {
            //     gameObject.ChangeAllMaterialsInChildren(renderers.ToArray(), previewMaterial);

            //     gameObject.ChangeAllMaterialsColorInChildren(renderers.ToArray(),
            //         MapCreator.GetInstance().AllowEdition ? MapCreatorManager.GetInstance().PreviewAllowedColor : MapCreatorManager.GetInstance().PreviewDeniedColor);

            //     foreach (GameObject Obj in objectsToDisableOnPreview)
            //         if (Obj)
            //             Obj.SetActive(false);

            //     foreach (MonoBehaviour Behaviour in behavioursToDisableOnPreview)
            //         if (Behaviour)
            //             Behaviour.enabled = false;

            //     EnableAllColliders();

            //     foreach (Collider Collider in collidersToDisableOnPreview)
            //         if (Collider)
            //             Collider.enabled = false;

            //     foreach (PartSocket Socket in sockets)
            //     {
            //         Socket.EnableCollider();

            //         Socket.gameObject.SetActive(true);
            //     }
            // }
            // else if (state == PartState.Remove)
            // {
            //     gameObject.ChangeAllMaterialsInChildren(renderers.ToArray(), previewMaterial);

            //     gameObject.ChangeAllMaterialsColorInChildren(renderers.ToArray(), MapCreatorManager.GetInstance().PreviewDeniedColor);

            //     foreach (GameObject Obj in objectsToDisableOnPreview)
            //         if (Obj)
            //             Obj.SetActive(false);

            //     foreach (MonoBehaviour Behaviour in behavioursToDisableOnPreview)
            //         if (Behaviour)
            //             Behaviour.enabled = false;

            //     EnableAllColliders();

            //     foreach (Collider Collider in collidersToDisableOnPreview)
            //         if (Collider)
            //             Collider.enabled = false;

            //     foreach (PartSocket Socket in sockets)
            //     {
            //         Socket.DisableCollider();

            //         Socket.gameObject.SetActive(false);
            //     }
            // }
            // else if (state == PartState.Placed)
            // {
            //     gameObject.ChangeAllMaterialsInChildren(renderers.ToArray(), initialRenders);

            //     foreach (GameObject Obj in objectsToDisableOnPreview)
            //         if (Obj)
            //             Obj.SetActive(true);

            //     foreach (MonoBehaviour Behaviour in behavioursToDisableOnPreview)
            //         if (Behaviour)
            //             Behaviour.enabled = false;

            //     EnableAllColliders();

            //     foreach (Collider Collider in collidersToDisableOnPreview)
            //         if (Collider)
            //             Collider.enabled = true;

            //     foreach (PartSocket Socket in sockets)
            //     {
            //         Socket.EnableCollider();

            //         Socket.gameObject.SetActive(true);
            //     }
            // }

            // currentState = state;

            // MapCreatorEvents.PartStateChanged(this, state);
        }

        /// <summary>
        /// Change the state of all the sockets which hit the part bounds.
        /// </summary>
        public void ChangeAreaState(OccupancyType type)
        {
            PartSocket[] sockets = Searcher.GetComponentsInBoxArea<PartSocket>(GetWorldPartMeshBounds().center, GetWorldPartMeshBounds().extents,
                transform.rotation, LayerMask.NameToLayer(Constants.LAYER_SOCKET.ToLower()), QueryTriggerInteraction.Collide);

            for (int i = 0; i < sockets.Length; i++)
            {
                if (sockets[i] != null)
                {
                    if (sockets[i].socketOwner != this)
                    {
                        if (sockets[i].AllowPart(this) && type == OccupancyType.Busy)
                        {
                            sockets[i].ChangeAreaState(type,
                                LayerMask.NameToLayer(Constants.LAYER_DEFAULT.ToLower()),
                                LayerMask.NameToLayer(Constants.LAYER_SOCKET.ToLower()), sockets[i].socketOwner);

                            sockets[i].ChangeState(type, this);
                        }
                        else if (type == OccupancyType.Free)
                        {
                            sockets[i].ChangeAreaState(type,
                                LayerMask.NameToLayer(Constants.LAYER_DEFAULT.ToLower()),
                                LayerMask.NameToLayer(Constants.LAYER_SOCKET.ToLower()), sockets[i].socketOwner);

                            sockets[i].ChangeState(type, this);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This method allows to active all the trigger of collider(s).
        /// </summary>
        public void ActiveAllTriggers()
        {
            foreach (Collider Coll in partColliders)
                Coll.isTrigger = true;
        }

        /// <summary>
        /// This method allows to disable all the trigger of collider(s).
        /// </summary>
        public void DisableAllTriggers()
        {
            foreach (Collider Coll in partColliders)
                Coll.isTrigger = false;
        }

        /// <summary>
        /// This method allows to enable all the collider(s).
        /// </summary>
        public void EnableAllColliders()
        {
            foreach (Collider Coll in partColliders)
                Coll.enabled = true;
        }

        /// <summary>
        /// This method allows to disable all the collider(s).
        /// </summary>
        public void DisableAllColliders()
        {
            foreach (Collider Coll in partColliders)
                Coll.enabled = false;
        }

        /// <summary>
        /// This method allows to enable all the child socket(s).
        /// </summary>
        public void EnableAllSockets()
        {
            foreach (PartSocket Socket in sockets)
                Socket.EnableCollider();
        }

        /// <summary>
        /// This method allows to disable all the child socket(s).
        /// </summary>
        public void DisableAllSockets()
        {
            foreach (PartSocket Socket in sockets)
                Socket.DisableCollider();
        }

        /// <summary>
        /// This method allows to check if the part is stable.
        /// </summary>
        public bool CheckStability()
        {
            return true;//TODO: delete it
            // if (MapCreatorManager.GetInstance() != null)
            //     if (!MapCreatorManager.GetInstance().UsePhysics)
            //         return true;

            // if (!advancedFeatures)
            //     return true;

            // if (!useConditionalPhysics)
            //     return true;

            // if (CustomDetections.Length != 0)
            // {
            //     bool[] Results = new bool[CustomDetections.Length];

            //     for (int i = 0; i < CustomDetections.Length; i++)
            //     {
            //         if (CustomDetections[i] != null)
            //         {
            //             Part[] Parts = Searcher.GetComponentsInBoxArea<Part>(transform.TransformPoint(CustomDetections[i].Position),
            //                 CustomDetections[i].Size, transform.rotation, PhysicsLayers);

            //             for (int p = 0; p < Parts.Length; p++)
            //             {
            //                 Part Part = Parts[p].GetComponent<Part>();

            //                 if (Part != null)
            //                 {
            //                     if (Part != this)
            //                     {
            //                         if (!Part.AffectedByPhysics /*&& CustomDetections[i].CheckType((int)Part.Type)*/)
            //                         {
            //                             Results[i] = true;
            //                         }
            //                     }
            //                 }
            //             }

            //             Collider[] Colliders = Searcher.GetComponentsInBoxArea<Collider>(transform.TransformPoint(CustomDetections[i].Position),
            //                 CustomDetections[i].Size, transform.rotation, PhysicsLayers);

            //             for (int x = 0; x < Colliders.Length; x++)
            //             {
            //                 // bool UseTerrain = Application.isPlaying == true ? MapCreatorManager.GetInstance().BuildingSupport == SupportType.Terrain
            //                 //     : FindObjectOfType<MapCreatorManager>().BuildingSupport == SupportType.Terrain;

            //                 // if (UseTerrain)
            //                 // {
            //                 //     // if (CustomDetections[i].RequiredSupports.Contains(SurfaceType.SurfaceAndTerrain))
            //                 //     // {
            //                 //     //     if (CustomDetections[i].RequiredSupports.ToList().Find(entry => entry == SurfaceType.SurfaceAndTerrain) == SurfaceType.SurfaceAndTerrain)
            //                 //     //     {
            //                 //     //         if (Colliders[x] as TerrainCollider)
            //                 //     //         {
            //                 //     //             Results[i] = true;
            //                 //     //         }
            //                 //     //     }
            //                 //     // }
            //                 // }
            //                 // else
            //                 // {
            //                 //     // if (CustomDetections[i].RequiredSupports.ToList().Contains(SurfaceType.SurfaceAndTerrain))
            //                 //     // {
            //                 //     //     if (Colliders[x].GetComponent<SurfaceCollider>() != null)
            //                 //     //     {
            //                 //     //         Results[i] = true;
            //                 //     //     }
            //                 //     // }
            //                 // }
            //             }
            //         }
            //     }

            //     return Results.All(result => result);
            // }

            // return false;
        }

        // /// <summary>
        // /// This method allows to check the condition of close area(s).
        // /// </summary>
        // public bool CheckAreas()
        // {
        //     AreaBehaviour NearestArea = MapCreatorManager.GetInstance().GetNearestArea(transform.position);

        //     if (NearestArea != null)
        //     {
        //         if (!NearestArea.AllowPlacement)
        //             return true;
        //         else
        //         {
        //             if (NearestArea.AllowPartPlacement.Count != 0 && !NearestArea.CheckAllowedPart(this))
        //                 return true;
        //         }
        //     }

        //     return false;
        // }

        /// <summary>
        /// This method allows to apply the physics on this part.
        /// </summary>
        public void ApplyPhysics()
        {
            // if (!MapCreatorManager.GetInstance().UsePhysics)
            //     return;

            if (!advancedFeatures)
                return;

            if (!useConditionalPhysics)
                return;

            if (AffectedByPhysics)
                return;

            for (int i = 0; i < PhysicsIgnoreTags.Length; i++)
            {
                GameObject[] IgnoreCollider = GameObject.FindGameObjectsWithTag(PhysicsIgnoreTags[i]);

                for (int x = 0; x < IgnoreCollider.Length; x++)
                    if (IgnoreCollider[x].GetComponent<Collider>() != null)
                        for (int y = 0; y < partColliders.Count; y++)
                            Physics.IgnoreCollision(partColliders[y], IgnoreCollider[x].GetComponent<Collider>());
            }

            if (GetComponent<Rigidbody>() != null)
            {
                if (PhysicsConvexOnAffected)
                {
                    for (int i = 0; i < partColliders.Count; i++)
                        if (partColliders[i].GetComponent<MeshCollider>() != null)
                            partColliders[i].GetComponent<MeshCollider>().convex = true;
                }

                GetComponent<Rigidbody>().isKinematic = false;
            }

            ChangeAreaState(OccupancyType.Free);

            AffectedByPhysics = true;

            DisableAllSockets();

            Destroy(this);

            Destroy(gameObject, PhysicsLifeTime);
        }

        // /// <summary>
        // /// This method allows to check if the part enters in the terrain.
        // /// </summary>
        // public bool CheckTerrainClipping()
        // {
        //     if (!advancedFeatures)
        //         return false;

        //     if (!UseTerrainPrevention)
        //         return false;

        //     Collider[] Colliders = Searcher.GetComponentsInBoxArea<Collider>(GetWorldPartTerrainBounds().center,
        //         TerrainBounds.extents, transform.rotation, Physics.AllLayers);

        //     for (int i = 0; i < Colliders.Length; i++)
        //         if (Colliders[i] as TerrainCollider || Colliders[i].GetComponentInParent<VoxelandCollider>())
        //             return true;

        //     return false;
        // }

        /// <summary>
        /// This method allows to check if the part enters an element(s).
        /// </summary>
        public bool CheckElementsClipping(LayerMask defaultLayer)
        {
            Collider[] Colliders = Searcher.GetComponentsInBoxArea<Collider>(GetWorldPartMeshBounds().center,
                GetWorldPartMeshBounds().extents, transform.rotation, defaultLayer);

            for (int i = 0; i < Colliders.Length; i++)
                if (Colliders[i] != null)
                    if (!IsSupport(Colliders[i]))
                        return true;

            return false;
        }

        /// <summary>
        /// This method allows to check if the part enters an element(s) when the preview is placed on an socket.
        /// </summary>
        public bool CheckElementsClippingOnSocket(LayerMask defaultLayer)
        {
            Collider[] Colliders = Searcher.GetComponentsInBoxArea<Collider>(GetWorldPartMeshBounds().center,
                GetWorldPartMeshBounds().extents, transform.rotation, defaultLayer);

            for (int i = 0; i < Colliders.Length; i++)
                if (Colliders[i] != null)
                    if (!IsSupport(Colliders[i]) && Colliders[i].GetComponentInParent<Part>() == null)
                        return true;

            Colliders = Searcher.GetComponentsInBoxArea<Collider>(GetWorldPartMeshBounds().center,
                GetWorldPartMeshBounds().extents / 3.5f, transform.rotation, defaultLayer);

            for (int i = 0; i < Colliders.Length; i++)
                if (Colliders[i] != null)
                    if (!IsSupport(Colliders[i]) && Colliders[i].GetComponentInParent<Part>() != null)
                        return true;

            return false;
        }

        /// <summary>
        /// This method allows to check if the collider is a support.
        /// </summary>
        public bool IsSupport(Collider collider)
        {
            // if (MapCreatorManager.GetInstance().BuildingSupport == SupportType.All)
            //     return true;
            // else if (MapCreatorManager.GetInstance().BuildingSupport == SupportType.Terrain)
            //     return collider.GetComponent<TerrainCollider>();
            // else if (MapCreatorManager.GetInstance().BuildingSupport == SupportType.Voxeland)
            //     return collider.GetComponentInParent<VoxelandCollider>();
            // else if (MapCreatorManager.GetInstance().BuildingSupport == SupportType.Surface)
            //     return collider.GetComponent<SurfaceCollider>();
            //else
                return false;
        }

        /// <summary>
        /// This method allows to link a part at this part.
        /// </summary>
        public void LinkPart(Part part)
        {
            if (!linkedParts.Contains(part))
            {
                if (part.useConditionalPhysics)
                {
                    if (part.CheckStability())
                        linkedParts.Add(part);
                }
                else
                    linkedParts.Add(part);
            }
        }

        /// <summary>
        /// This method allows to unlink a part at this part.
        /// </summary>
        public void UnlinkPart(Part part)
        {
            linkedParts.Remove(part);
        }

        /// <summary>
        /// This method allows to check if the collider is attached in this part.
        /// </summary>
        public bool HasCollider(Collider collider)
        {
            for (int i = 0; i < partColliders.Count; i++)
                if (partColliders[i] == collider)
                    return true;

            return false;
        }

/*
        /// <summary>
        /// This method allows to change the part appearance by index.
        /// </summary>
        public void ChangeAppearance(int appearanceIndex)
        {
            if (!advancedFeatures)
                return;

            if (!useAppearances)
                return;

            if (AppearanceIndex == appearanceIndex)
                return;

            if (Appearances.Count < appearanceIndex)
                return;

            AppearanceIndex = appearanceIndex;

            EventHandlers.ChangedAppearance(this, appearanceIndex);
        }
*/

        /// <summary>
        /// This method allows to get the part bounds in world space.
        /// </summary>
        public Bounds GetWorldPartMeshBounds() { return transform.BoundsToWorld(meshBounds); }

        /// <summary>
        /// This method allows to get the terrain bounds in world space.
        /// </summary>
        public Bounds GetWorldPartTerrainBounds() { return transform.BoundsToWorld(TerrainBounds); }

        #endregion Public Methods
    }
}
