﻿using System.Collections;
using System.Collections.Generic;
using MapCreatorUtility.Helpers;
using MapCreatorUtility.Helpers.Extensions;
using MapCreatorUtility.Runtimes.Events;
using MapCreatorUtility.Runtimes.Managers;
using MapCreatorUtility.Runtimes.Parts;
using MapCreatorUtility.Runtimes.Sockets;
using MapCreatorUtility.Values;
using UnityEditor;
using UnityEngine;

namespace MapCreatorUtility.Runtimes.MainBuilder
{
    public enum BuildMode
    {
        None,
        Placement,
        Destruction,
        Edition
    }

    public enum DetectionType
    {
        Line,
        Overlap
    }

    public enum MovementType
    {
        Normal,
        Smooth,
        Grid
    }

    public class MapCreator
    {

        #region public fields

        #region Base Settings

        public float actionDistance = 6f;

        public float OutOfRangeDistance = 0f;

        public float OverlapAngles = 35f;

        public bool LockRotation;

        public LayerMask FreeLayers;

        public LayerMask SocketLayers;

        public DetectionType RayDetection = DetectionType.Overlap;

        public float RaycastOffset = 1f;

        public Transform RaycastOriginTransform;

        public Transform RaycastAnchorTransform;

        #endregion Base Settings

        #region Modes Settings

        public bool UsePlacementMode = true;
        public bool ResetModeAfterPlacement = false;
        public bool UseDestructionMode = true;
        public bool ResetModeAfterDestruction = false;
        public bool UseEditionMode = true;
        public bool ResetModeAfterEdition = false;

        #endregion Modes Settings

        #region Preview Settings

        public bool UsePreviewCamera;
        public Camera PreviewCamera;
        public LayerMask PreviewLayer;

        public MovementType PreviewMovementType;

        public float PreviewGridSize = 1.0f;
        public float PreviewGridOffset;
        public float PreviewSmoothTime = 5.0f;

        #endregion Preview Settings

        #region Audio Settings

        public AudioSource Source;

        public AudioClip[] PlacementClips;

        public AudioClip[] DestructionClips;

        #endregion Audio Settings

        [HideInInspector]
        public BuildMode activeBuildMode;

        [HideInInspector]
        public Part SelectedPrefab;

        [HideInInspector]
        public int SelectedIndex;

        [HideInInspector]
        public Part CurrentPreview;

        [HideInInspector]
        public Part CurrentEditionPreview;

        [HideInInspector]
        public Part CurrentRemovePreview;

        [HideInInspector]
        public Vector3 CurrentRotationOffset;

        [HideInInspector]
        public bool AllowPlacement = true;

        [HideInInspector]
        public bool AllowDestruction = true;

        [HideInInspector]
        public bool AllowEdition = true;

        [HideInInspector]
        public bool HasSocket;

        [HideInInspector]
        public PartSocket CurrentSocket;

        [HideInInspector]
        public PartSocket LastSocket;

        // public Transform GetTransform
        // {
        //     get
        //     {
        //         return CameraType != RayType.TopDown ? transform.parent != null ? transform.parent
        //             : transform : RaycastAnchorTransform != null ? RaycastAnchorTransform : transform;
        //     }
        // }

        #endregion

        #region private fields
        private static MapCreator singleton;
        private static bool mapCreatorIsEnabled = false;

        private LogChannel log = new LogChannel("MapCreator");

        private Camera editorCamera;

        private RaycastHit rayHit;
        #endregion

        #region INITIALIZATION / SERIALIZATION

        private MapCreator()
        {
        }

        /// <summary>
        /// Starts Map creator utility if it's enabled
        /// </summary>
        public static void StartIfEnabled()
        {
            if (!EditorApplication.isPlayingOrWillChangePlaymode && EditorPrefs.GetBool(PreferenceKeys.MAP_CREATOR_IS_ENABLED, false))
                Start();
        }

        /// <summary>
        /// Starts and enables Map creator utility
        /// </summary>
        public static void Start()
        {
            if (singleton == null)
                singleton = new MapCreator();

            singleton.Initialize();

            EditorPrefs.SetBool(PreferenceKeys.MAP_CREATOR_IS_ENABLED, true);
            mapCreatorIsEnabled = true;

            MapCreatorEvents.MapCreatorEnabled(singleton);
        }

        public static void CloseIfEnabled()
        {
            if (singleton != null)
                singleton.Uninitialize();

            mapCreatorIsEnabled = false;
            MapCreatorEvents.MapCreatorDisabled();
        }

        public static void Close()
        {
            EditorPrefs.SetBool(PreferenceKeys.MAP_CREATOR_IS_ENABLED, false);
            mapCreatorIsEnabled = false;

            if (singleton != null)
                singleton.Uninitialize();

            MapCreatorEvents.MapCreatorDisabled();
        }

        private void Initialize()
        {
            LoadPreferences();
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
            SceneView.onSceneGUIDelegate += OnSceneGUI;

            SceneView.RepaintAll();
        }

        private void Uninitialize()
        {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
            SceneView.RepaintAll();
        }

        public static bool IsEnabled()
        {
            return singleton != null && mapCreatorIsEnabled;
        }

        #endregion

        #region Private Methods

        private void OnSceneGUI(SceneView sceneView)
        {
            Event currentEvent = Event.current;

            SetEditorCamera(sceneView);

            //if (!EditorApplication.isPlayingOrWillChangePlaymode) ;
            /*DoTransformSnapping(currentEvent, view.camera);*/
        }

        private void MapCreatorUpdate()
        {

        }

        //можно перенести в какой нибудь editorUtility
        private void SetEditorCamera(SceneView sceneView)
        {
            if (editorCamera != null)
            {
                return;
            }

            editorCamera = sceneView.camera;

            if (editorCamera == null)
            {
                log.LogWarning("Failed to find editor camera. Retrying to find...");
            }
        }

        private void LoadPreferences()
        {
            //mapCreatorIsEnabled = EditorPrefs.GetBool(PreferenceKeys.MAP_CREATOR_IS_ENABLED, false);
        }

        private void OnEnable()
        {
            if (!Application.isPlaying)
            {
                if (FreeLayers == 0)
                    FreeLayers = LayerMask.GetMask(Constants.LAYER_DEFAULT);

                if (SocketLayers == 0)
                    SocketLayers = LayerMask.GetMask(Constants.LAYER_SOCKET);
            }
        }

        // private void Start()
        // {
        //     if (!Application.isPlaying)
        //         return;

        //     Instance = this;

        //     if (PreviewCamera != null)
        //         PreviewCamera.enabled = UsePreviewCamera;

        //     editorCamera = GetComponent<Camera>();

        //     if (editorCamera == null)
        //         Debug.Log("<b><color=cyan>[Easy Build System]</color></b> : No camera for the Builder Behaviour component.");
        // }

        private void Update()
        {
            // if (!Application.isPlaying)
            //     return;

            UpdateModes();
        }

        private Ray GetRay()
        {
            if (editorCamera != null)
                return editorCamera.ScreenPointToRay(Input.mousePosition);
            else
                return new Ray();
        }

        #endregion


        #region Public Methods

        public static MapCreator GetInstance()
        {
            return singleton;
        }

        public Vector3 getEditorCameraPosition()
        {
            if (editorCamera != null)
                return editorCamera.transform.position;
            else
                return Vector3.zero;
        }

        /// <summary>
        /// This method allows to update all the builder (Placement, Destruction, Edition).
        /// </summary>
        public virtual void UpdateModes()
        {
            if (MapCreatorManager.GetInstance() == null)
                return;

            // if (MapCreatorManager.GetInstance().PartsCollection == null) //Useless now
            //     return;

            if (activeBuildMode == BuildMode.Placement)
            {
                if (SelectedPrefab == null)
                    return;

                if (!PreviewExists())
                {
                    CreatePreview(SelectedPrefab.gameObject);
                    return;
                }
                else
                {
                    UpdatePreview();
                }
            }
            else if (activeBuildMode == BuildMode.Destruction)
                UpdateRemovePreview();
            else if (activeBuildMode == BuildMode.Edition)
                UpdateEditionPreview();
            else if (activeBuildMode == BuildMode.None)
                ClearPreview();
        }

        #region Placement

        /// <summary>
        /// This method allows to update the placement preview.
        /// </summary>
        public void UpdatePreview()
        {
            HasSocket = false;

            // if (CameraType == RayType.TopDown)
            //     Physics.Raycast(GetRay(), out TopDownHit, Mathf.Infinity, LayerMask.NameToLayer(Constants.LAYER_SOCKET.ToLower()));

            if (RayDetection == DetectionType.Overlap)
            {
                PartSocket[] Sockets =
                    Searcher.GetComponentsInSphereArea<PartSocket>(editorCamera.transform.position, actionDistance,
                    LayerMask.NameToLayer(Constants.LAYER_SOCKET.ToLower()));

                if (Sockets.Length > 0)
                    UpdateSnapsMovement(Sockets);
                else
                    UpdateFreeMovement();
            }
            else
            {
                PartSocket Socket = null;

                RaycastHit Hit;

                if (Physics.Raycast(GetRay(), out Hit, actionDistance, LayerMask.NameToLayer(Constants.LAYER_DEFAULT.ToLower())))
                    if (Hit.collider.GetComponentInChildren<PartSocket>() != null)
                        Socket = Hit.collider.GetComponentInChildren<PartSocket>();

                if (Socket != null)
                    UpdateSnapsMovement(new PartSocket[1] { Socket });
                else
                    UpdateFreeMovement();
            }

            if (activeBuildMode == BuildMode.Placement || activeBuildMode == BuildMode.Edition)
            {
                foreach (PartSocket Socket in MapCreatorManager.GetInstance().sockets)
                    if (Socket != null)
                        Socket.EnableColliderByPartType(SelectedPrefab.Type);
            }

            UpdatePreviewCollisions();

            CurrentPreview.gameObject.ChangeAllMaterialsColorInChildren(CurrentPreview.renderers.ToArray(),
                AllowPlacement ? MapCreatorManager.GetInstance().PreviewAllowedColor : MapCreatorManager.GetInstance().PreviewDeniedColor, SelectedPrefab.PreviewColorLerpTime, SelectedPrefab.PreviewUseColorLerpTime);
        }

        /// <summary>
        /// This method allows to move the preview in free movement.
        /// </summary>
        public void UpdateFreeMovement()
        {
            if (CurrentPreview == null)
                return;

            RaycastHit Hit;

            float Distance = OutOfRangeDistance == 0 ? actionDistance : OutOfRangeDistance;

            if (Physics.Raycast(GetRay(), out Hit, Distance, FreeLayers))
            {
                if (PreviewMovementType == MovementType.Normal)
                    CurrentPreview.transform.position = Hit.point + CurrentPreview.PreviewOffset;
                else if (PreviewMovementType == MovementType.Grid)
                    CurrentPreview.transform.position = MathExtension.PositionToGridPosition(PreviewGridSize, PreviewGridOffset, Hit.point + CurrentPreview.PreviewOffset);
                else if (PreviewMovementType == MovementType.Smooth)
                    CurrentPreview.transform.position = Vector3.Lerp(CurrentPreview.transform.position, Hit.point + CurrentPreview.PreviewOffset, PreviewSmoothTime * Time.deltaTime);

                if (!CurrentPreview.RotateAccordingSlope)
                {
                    if (LockRotation)
                        CurrentPreview.transform.rotation = editorCamera.transform.rotation * SelectedPrefab.transform.localRotation * Quaternion.Euler(CurrentPreview.RotationAxis + CurrentRotationOffset);
                    else
                        CurrentPreview.transform.rotation = Quaternion.Euler(CurrentPreview.RotationAxis + CurrentRotationOffset);
                }
                else
                {
                    Quaternion SlopeRotation = Quaternion.identity;

                    if (Hit.collider is TerrainCollider)
                    {
                        float SampleHeight = Hit.collider.GetComponent<UnityEngine.Terrain>().SampleHeight(Hit.point);

                        if (Hit.point.y - .1f < SampleHeight)
                        {
                            CurrentPreview.transform.rotation = Quaternion.FromToRotation(editorCamera.transform.up, Hit.normal) * Quaternion.Euler(CurrentRotationOffset)
                                * editorCamera.transform.rotation * SelectedPrefab.transform.localRotation * Quaternion.Euler(CurrentPreview.RotationAxis + CurrentRotationOffset);
                        }
                        else
                            CurrentPreview.transform.rotation = editorCamera.transform.rotation * SelectedPrefab.transform.localRotation * Quaternion.Euler(CurrentPreview.RotationAxis + CurrentRotationOffset);
                    }
                    else
                        CurrentPreview.transform.rotation = editorCamera.transform.rotation * SelectedPrefab.transform.localRotation * Quaternion.Euler(CurrentPreview.RotationAxis + CurrentRotationOffset);
                }

                return;
            }

            if (LockRotation)
                CurrentPreview.transform.rotation = editorCamera.transform.rotation * SelectedPrefab.transform.localRotation * Quaternion.Euler(CurrentPreview.RotationAxis + CurrentRotationOffset);
            else
                CurrentPreview.transform.rotation = Quaternion.Euler(CurrentPreview.RotationAxis + CurrentRotationOffset);

            Transform StartTransform = (CurrentPreview.GroundUpperHeight == 0) ? editorCamera.transform : editorCamera.transform;

            Vector3 LookDistance = StartTransform.position + StartTransform.forward * Distance;

            if (CurrentPreview.UseGroundUpper)
            {
                LookDistance.y = Mathf.Clamp(LookDistance.y, editorCamera.transform.position.y - CurrentPreview.GroundUpperHeight,
                    editorCamera.transform.position.y + CurrentPreview.GroundUpperHeight);
            }
            else
            {
                if (!CurrentPreview.FreePlacement)
                {
                    if (Physics.Raycast(CurrentPreview.transform.position + new Vector3(0, 0.3f, 0),
                        Vector3.down, out Hit, Mathf.Infinity, FreeLayers, QueryTriggerInteraction.Ignore))
                        LookDistance.y = Hit.point.y;
                }
                else
                    LookDistance.y = Mathf.Clamp(LookDistance.y, editorCamera.transform.position.y,
                        editorCamera.transform.position.y);
            }

            if (PreviewMovementType == MovementType.Normal)
                CurrentPreview.transform.position = LookDistance;
            else if (PreviewMovementType == MovementType.Grid)
                CurrentPreview.transform.position = MathExtension.PositionToGridPosition(PreviewGridSize, PreviewGridOffset, LookDistance + CurrentPreview.PreviewOffset);
            else if (PreviewMovementType == MovementType.Smooth)
                CurrentPreview.transform.position = Vector3.Lerp(CurrentPreview.transform.position, LookDistance, PreviewSmoothTime * Time.deltaTime);

            CurrentSocket = null;

            LastSocket = null;

            HasSocket = false;
        }

        /// <summary>
        /// This method allows to move the preview only on available socket(s).
        /// </summary>
        public void UpdateSnapsMovement(PartSocket[] sockets)
        {
            if (CurrentPreview == null)
                return;

            float ClosestAngle = Mathf.Infinity;

            CurrentSocket = null;

            for (int i = 0; i < sockets.Length; i++)
            {
                Part Part = sockets[i].socketOwner;

                if (Part == null || Part.sockets.Length == 0)
                    continue;

                for (int x = 0; x < Part.sockets.Length; x++)
                {
                    PartSocket Socket = Part.sockets[x];

                    if (Socket != null)
                    {
                        if (Socket.gameObject.activeSelf && !Socket.isDisabled)
                        {
                            if (Socket.AllowPart(SelectedPrefab) && !Part.AvoidAnchoredOnSocket)
                            {
                                if (RayDetection == DetectionType.Overlap)
                                {
                                    if ((Socket.transform.position - editorCamera.transform.position).sqrMagnitude < Mathf.Pow(actionDistance, 2))
                                    {
                                        float Angle = Vector3.Angle(GetRay().direction, Socket.transform.position - GetRay().origin);

                                        if (Angle < ClosestAngle && Angle < OverlapAngles)
                                        {
                                            ClosestAngle = Angle;

                                            CurrentSocket = Socket;
                                        }
                                    }
                                }
                                else
                                {
                                    CurrentSocket = Socket;
                                }
                            }
                        }
                    }
                }
            }

            if (CurrentSocket != null)
            {
                PartOffset Offset = CurrentSocket.GetOffsetPart(SelectedPrefab.Id);

                if (CurrentSocket.CheckOccupancy(SelectedPrefab))
                    return;

                if (Offset != null)
                {
                    CurrentPreview.transform.position = CurrentSocket.transform.position + CurrentSocket.transform.TransformVector(Offset.Position);

                    CurrentPreview.transform.rotation = CurrentSocket.transform.rotation * (CurrentPreview.RotateOnSockets ? Quaternion.Euler(Offset.Rotation + CurrentRotationOffset) : Quaternion.Euler(Offset.Rotation));

                    if (Offset.UseCustomScale)
                        CurrentPreview.transform.localScale = Offset.Scale;

                    LastSocket = CurrentSocket;

                    HasSocket = true;

                    return;
                }
            }

            UpdateFreeMovement();
        }

        /// <summary>
        /// This method allows to update the preview collisions.
        /// </summary>
        protected void UpdatePreviewCollisions()
        {
            bool HasCollision = false;//= CurrentPreview.CheckTerrainClipping();

            if (CurrentPreview.AvoidClipping && !HasCollision && !HasSocket)
                HasCollision = CurrentPreview.CheckElementsClipping(FreeLayers);
            else if (CurrentPreview.AvoidClippingOnSocket && !HasCollision && HasSocket)
                HasCollision = CurrentPreview.CheckElementsClippingOnSocket(FreeLayers);

            // if (!HasCollision)
            //     HasCollision = CurrentPreview.CheckAreas();

            if (HasSocket)
                AllowPlacement = !HasCollision;
            else
                AllowPlacement = !CurrentPreview.RequireSockets && !HasCollision;

            if (!HasCollision && AllowPlacement)
                if (OutOfRangeDistance != 0)
                    AllowPlacement = Vector3.Distance(editorCamera.transform.position, CurrentPreview.transform.position) < actionDistance;

            if (!HasCollision && CurrentPreview.useConditionalPhysics && CurrentPreview.PhysicsOnlyStablePlacement)
                AllowPlacement = AllowPlacement && CurrentPreview.CheckStability();

            RaycastHit Hit;

            if (!CurrentPreview.FreePlacement)
            {
                if (CurrentPreview.Type == PartType.Foundation)
                {
                    if (!HasSocket)
                    {
                        if (Physics.Raycast(CurrentPreview.transform.position + new Vector3(0, 0.1f, 0), Vector3.down, out Hit,
                            CurrentPreview.UseGroundUpper ? CurrentPreview.GroundUpperHeight : 0.3f, FreeLayers))
                        {
                            // if (TerrainManager.Instance != null)
                            // {
                            //     if (TerrainManager.Instance.ActiveTerrain != null)
                            //     {
                            //         if (BuildManager.Instance.BuildingSupport == SupportType.Terrain)
                            //         {
                            //             if (Hit.collider as TerrainCollider == null)
                            //             {
                            //                 if (AllowPlacement)
                            //                     AllowPlacement = false;
                            //             }
                            //         }
                            //     }
                            // }
                            // else
                            // {
                            //     if (MapCreatorManager.GetInstance().BuildingSupport == SupportType.Voxeland)
                            //     {
                            //         if (Hit.collider.GetComponentInParent<VoxelandCollider>() == null)
                            //         {
                            //             if (AllowPlacement)
                            //                 AllowPlacement = false;
                            //         }
                            //     }
                            //     else if (MapCreatorManager.GetInstance().BuildingSupport == SupportType.Surface)
                            //     {
                            //         if (Hit.collider.GetComponent<SurfaceCollider>() == null)
                            //         {
                            //             if (AllowPlacement)
                            //                 AllowPlacement = false;
                            //         }
                            //     }
                            // }
                        }
                        else
                        {
                            if (AllowPlacement)
                                AllowPlacement = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This method allows to place the current preview.
        /// </summary>
        public virtual void PlacePrefab()
        {
            if (!AllowPlacement)
                return;

            if (CurrentPreview == null)
                return;

            if (CurrentEditionPreview != null)
                Object.DestroyImmediate(CurrentEditionPreview.gameObject);

            MapCreatorManager.GetInstance().PlacePrefab(SelectedPrefab,
                CurrentPreview.transform.position,
                CurrentPreview.transform.eulerAngles,
                CurrentPreview.transform.localScale,
                null,
                CurrentSocket);

            if (Source != null)
                if (PlacementClips.Length != 0)
                    Source.PlayOneShot(PlacementClips[Random.Range(0, PlacementClips.Length)]);

            CurrentRotationOffset = Vector3.zero;

            CurrentSocket = null;

            LastSocket = null;

            AllowPlacement = false;

            HasSocket = false;

            if (ResetModeAfterPlacement || ResetModeAfterEdition)
                ChangeMode(BuildMode.None);

            if (CurrentPreview != null)
                Object.DestroyImmediate(CurrentPreview.gameObject);
        }

        /// <summary>
        /// This method allows to create a preview.
        /// </summary>
        public virtual Part CreatePreview(GameObject prefab)
        {
            if (prefab == null)
                return null;

            CurrentPreview = Object.Instantiate(prefab).GetComponent<Part>();

            RaycastHit Hit;

            if (Physics.Raycast(GetRay(), out Hit, Mathf.Infinity, FreeLayers, QueryTriggerInteraction.Ignore))
                CurrentPreview.transform.position = Hit.point;

            CurrentPreview.ChangeState(PartState.Preview);

            SelectedPrefab = prefab.GetComponent<Part>();

            if (UsePreviewCamera == true)
                CurrentPreview.gameObject.SetLayerRecursively(PreviewLayer);

            MapCreatorEvents.PartPreviewCreated(CurrentPreview);

            CurrentSocket = null;

            LastSocket = null;

            AllowPlacement = false;

            HasSocket = false;

            return CurrentPreview;
        }

        /// <summary>
        /// This method allows to clear the current preview.
        /// </summary>
        public virtual void ClearPreview()
        {
            if (CurrentPreview == null)
                return;

            MapCreatorEvents.PartPreviewCanceled(CurrentPreview);

            Object.DestroyImmediate(CurrentPreview.gameObject);

            AllowPlacement = false;

            CurrentPreview = null;
        }

        /// <summary>
        /// This method allows to check if the current preview exists.
        /// </summary>
        public bool PreviewExists()
        {
            return CurrentPreview;
        }

        #endregion Placement

        #region Destruction

        /// <summary>
        /// This method allows to update the destruction preview.
        /// </summary>
        public void UpdateRemovePreview()
        {
            RaycastHit Hit;

            foreach (PartSocket Socket in MapCreatorManager.GetInstance().sockets)
                if (Socket != null)
                    Socket.DisableCollider();

            float Distance = OutOfRangeDistance == 0 ? actionDistance : OutOfRangeDistance;

            if (CurrentRemovePreview != null)
            {
                // AreaBehaviour NearestArea = MapCreatorManager.GetInstance().GetNearestArea(CurrentRemovePreview.transform.position);

                // if (NearestArea != null)
                //     AllowDestruction = NearestArea.AllowDestruction;
                // else
                AllowDestruction = true;

                CurrentRemovePreview.ChangeState(PartState.Remove);

                if (Vector3.Distance(editorCamera.transform.position, CurrentRemovePreview.transform.position) < Distance)
                    return;

                AllowPlacement = false;
            }

            if (Physics.Raycast(GetRay(), out Hit, Distance, FreeLayers))
            {
                Part Part = Hit.collider.GetComponentInParent<Part>();

                if (Part != null)
                {
                    if (CurrentRemovePreview != null)
                    {
                        if (CurrentRemovePreview.GetInstanceID() != Part.GetInstanceID())
                        {
                            ClearRemovePreview();

                            CurrentRemovePreview = Part;
                        }
                    }
                    else
                        CurrentRemovePreview = Part;
                }
                else
                    ClearRemovePreview();
            }
            else
                ClearRemovePreview();
        }

        /// <summary>
        /// This method allows to remove the current preview.
        /// </summary>
        public virtual void RemovePrefab()
        {
            if (CurrentRemovePreview == null)
                return;

            if (!AllowDestruction)
                return;

            Object.DestroyImmediate(CurrentRemovePreview.gameObject);

            if (Source != null)
                if (DestructionClips.Length != 0)
                    Source.PlayOneShot(DestructionClips[Random.Range(0, DestructionClips.Length)]);

            CurrentSocket = null;

            LastSocket = null;

            AllowDestruction = false;

            HasSocket = false;

            if (ResetModeAfterDestruction)
                ChangeMode(BuildMode.None);
        }

        /// <summary>
        /// This method allows to clear the current remove preview.
        /// </summary>
        public virtual void ClearRemovePreview()
        {
            if (CurrentRemovePreview == null)
                return;

            CurrentRemovePreview.ChangeState(CurrentRemovePreview.LastState);

            AllowDestruction = false;

            CurrentRemovePreview = null;
        }

        #endregion Destruction

        #region Edition

        /// <summary>
        /// This method allows to update the edition mode.
        /// </summary>
        public void UpdateEditionPreview()
        {
            RaycastHit Hit;

            AllowEdition = CurrentEditionPreview;

            if (CurrentEditionPreview != null && AllowEdition)
                CurrentEditionPreview.ChangeState(PartState.Edit);

            float Distance = OutOfRangeDistance == 0 ? actionDistance : OutOfRangeDistance;

            if (Physics.Raycast(GetRay(), out Hit, Distance, FreeLayers))
            {
                Part Part = Hit.collider.GetComponentInParent<Part>();

                if (Part != null)
                {
                    if (CurrentEditionPreview != null)
                    {
                        if (CurrentEditionPreview.GetInstanceID() != Part.GetInstanceID())
                        {
                            ClearEditionPreview();

                            CurrentEditionPreview = Part;
                        }
                    }
                    else
                        CurrentEditionPreview = Part;
                }
                else
                {
                    ClearEditionPreview();
                }
            }
            else
                ClearEditionPreview();
        }

        /// <summary>
        /// This method allows to edit the current preview.
        /// </summary>
        public virtual void EditPrefab()
        {
            if (!AllowEdition)
                return;

            Part Part = CurrentEditionPreview;

            Part.ChangeState(PartState.Edit);

            MapCreatorEvents.PartEdited(CurrentEditionPreview, CurrentSocket);

            SelectPrefab(Part);

            //SelectedPrefab.AppearanceIndex = Part.AppearanceIndex;

            ChangeMode(BuildMode.Placement);
        }

        /// <summary>
        /// This method allows to clear the current edition preview.
        /// </summary>
        public void ClearEditionPreview()
        {
            if (CurrentEditionPreview == null)
                return;

            CurrentEditionPreview.ChangeState(CurrentEditionPreview.LastState);

            AllowEdition = false;

            CurrentEditionPreview = null;
        }

        #endregion Edition

        #region Miscs

        /// <summary>
        /// This method allows to change mode.
        /// </summary>
        public void ChangeMode(BuildMode mode)
        {
            if (activeBuildMode == mode)
                return;

            if (mode == BuildMode.Placement && !UsePlacementMode)
                return;

            if (mode == BuildMode.Destruction && !UseDestructionMode)
                return;

            if (mode == BuildMode.Edition && !UseEditionMode)
                return;

            if (activeBuildMode == BuildMode.Placement)
                ClearPreview();

            if (activeBuildMode == BuildMode.Destruction)
                ClearRemovePreview();

            if (mode == BuildMode.None)
            {
                ClearPreview();
                ClearRemovePreview();
                ClearEditionPreview();
            }

            activeBuildMode = mode;

            //MapCreatorEvents.BuildModeChanged(activeBuildMode);
        }

        /// <summary>
        /// This method allows to select a prefab.
        /// </summary>
        public void SelectPrefab(Part prefab)
        {
            if (prefab == null)
                return;

            SelectedPrefab = MapCreatorManager.GetInstance().GetPart(prefab.Id);
        }

        /// <summary>
        /// This method allows to rotate the current preview.
        /// </summary>
        public void RotatePreview(Vector3 rotateAxis)
        {
            if (CurrentPreview == null)
                return;

            CurrentRotationOffset += rotateAxis;
        }

        /// <summary>
        /// This method allows to get the object wich the camera is currently looking at.
        /// </summary>
        public Part GetTargetedPart()
        {
            RaycastHit hit;

            if (Physics.SphereCast(GetRay(), .1f, out hit, actionDistance, LayerMask.NameToLayer(Constants.LAYER_DEFAULT.ToLower())))
            {
                Part part = hit.collider.GetComponentInParent<Part>();

                if (part != null)
                    return part;
            }

            return null;
        }

        #endregion Miscs

        #endregion


    }
}
