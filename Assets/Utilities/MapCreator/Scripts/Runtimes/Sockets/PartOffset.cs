﻿using System.Collections;
using System.Collections.Generic;
using MapCreatorUtility.Runtimes.Parts;
using UnityEngine;

namespace MapCreatorUtility.Runtimes.Sockets
{

    [System.Serializable]
    public class PartOffset
    {
        #region Public Fields

        public Part Part;

        public Vector3 Position;

        public Vector3 Rotation;

        public bool UseCustomScale;

        public Vector3 Scale = Vector3.one;

        #endregion Public Fields

        #region Public Methods

        public PartOffset(Part part)
        {
            Part = part;
        }

        #endregion Public Methods
    }
}
