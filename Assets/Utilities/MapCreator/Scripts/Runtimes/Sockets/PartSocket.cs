﻿using MapCreatorUtility.Helpers;
using MapCreatorUtility.Helpers.Extensions;
using MapCreatorUtility.Runtimes.Parts;
using MapCreatorUtility.Values;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MapCreatorUtility.Runtimes.Sockets
{
    public enum OccupancyType
    {
        Free,
        Busy
    }

    public enum SocketType
    {
        Socket,
        Attachment
    }

    public class PartSocket : MonoBehaviour
    {
        #region Public Fields

        public SocketType type;//done

        public float radius = 0.5f;//done
        public Bounds attachmentBounds;

        //public bool DisableOnGroundContact;

        public List<PartOffset> PartOffsets = new List<PartOffset>();
        public List<Part> busySpaces = new List<Part>();

        public Part socketOwner;//done

        public bool isDisabled;//done

        #endregion Public Fields

        #region Private Methods

        //TODO: Перенести в метод инициализации, который будет вызываться в update (скрипт работает только 
        //в editor-е
        private void Awake()//done
        {
            socketOwner = GetComponentInParent<Part>();//done

            gameObject.layer = LayerMask.NameToLayer(Constants.LAYER_SOCKET);//done

            if (type == SocketType.Socket)//done
                gameObject.AddSphereCollider(radius);//done
            else//done
                gameObject.AddBoxCollider(attachmentBounds.extents, attachmentBounds.center);//done
        }

        //тоже самое, что и с AWAKE()
        private void Start()
        {
            //MapCreatorManager.GetInstance().AddSocket(this);//done

            ChangeAreaState(OccupancyType.Busy,
                LayerMask.NameToLayer(Constants.LAYER_DEFAULT.ToLower()),
                LayerMask.NameToLayer(Constants.LAYER_SOCKET.ToLower()), socketOwner);

            CheckContacts();
        }

        private void OnDestroy()
        {
            //MapCreatorManager.GetInstance().RemoveSocket(this);//done
        }

        private void OnDrawGizmos()
        {
            if (isDisabled)
                return;

            if (busySpaces.Count != 0)
            {
                Gizmos.color = Color.red;

                Gizmos.DrawCube(transform.position, Vector3.one / 6);
            }
            else
            {
                Gizmos.color = Color.cyan;

                Gizmos.DrawCube(transform.position, Vector3.one / 6);
            }

#if UNITY_EDITOR

            if (UnityEditor.Selection.activeGameObject != gameObject)
                return;

#endif

            if (type == SocketType.Socket)
            {
                Gizmos.DrawWireCube(transform.position, Vector3.one / 6);

                Gizmos.DrawWireSphere(transform.position, radius);
            }
            else
            {
                Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

                Gizmos.DrawWireCube(attachmentBounds.center, Vector3.one / 6);

                Gizmos.DrawWireCube(attachmentBounds.center, attachmentBounds.extents);
            }
        }

        private void CheckContacts()
        {
            Collider[] colliders = Searcher.GetComponentsInSphereArea<Collider>(transform.position, radius, LayerMask.NameToLayer(Constants.LAYER_SOCKET.ToLower()));

            for (int i = 0; i < colliders.Length; i++)
            {
                PartSocket socket = colliders[i].GetComponent<PartSocket>();

                if (socket != null)
                {
                    if (socket != this)
                    {
                        if (socket.AllowPart(socket.socketOwner))
                            for (int x = 0; x < busySpaces.Count; x++)
                                socket.AddOccupancy(busySpaces[x]);
                    }
                }
            }

            /*/if (DisableOnGroundContact)
            {
                colliders = Searcher.GetComponentsInSphereArea<Collider>(transform.position, radius,
                    LayerMask.NameToLayer(Constants.LAYER_DEFAULT.ToLower()));

                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i] as TerrainCollider || colliders[i].GetComponentInParent<VoxelandCollider>() ||
                        colliders[i].GetComponentInParent<VoxelandCollider>())
                        DisableCollider();
                }
            }*/
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Makes raycasts ignore the socket
        /// </summary>
        public void DisableCollider()//done
        {
            isDisabled = true;

            if (GetComponent<Collider>() != null)
                GetComponent<Collider>().gameObject.layer = Physics.IgnoreRaycastLayer;
        }

        /// <summary>
        /// This method allows to enable the collider of the socket.
        /// </summary>
        public void EnableCollider()
        {
            isDisabled = false;

            if (GetComponent<Collider>() != null)
                GetComponent<Collider>().gameObject.layer = LayerMask.NameToLayer(Constants.LAYER_SOCKET);
        }

        /// <summary>
        /// This method allows to enable the collider of the socket only with if a offsets part has the type.
        /// </summary>
        public void EnableColliderByPartType(PartType type)
        {
            if (PartOffsets.Count > 0)
            {
                for (int i = 0; i < PartOffsets.Count; i++)
                {
                    if (PartOffsets[i].Part != null && !CheckOccupancy(PartOffsets[i].Part))
                    {
                        if (PartOffsets[i].Part.Type == type)
                            EnableCollider();
                    }
                    else
                    {
                        EnableCollider();
                    }
                }
            }
            else
                DisableCollider();
        }

        /// <summary>
        /// This method allows to check if the part is allowed by this socket.
        /// </summary>
        public bool AllowPart(Part part)
        {
            for (int i = 0; i < PartOffsets.Count; i++)
                if (PartOffsets[i] != null)
                    if (PartOffsets[i].Part != null)
                        if (PartOffsets[i].Part.Id == part.Id && !CheckOccupancy(part))
                            return true;

            return false;
        }

        /// <summary>
        /// This method allows to check if this socket is busy or not.
        /// </summary>
        public OccupancyType CheckState(LayerMask freeLayers, Part placedPart)
        {
            Collider[] Colliders = Physics.OverlapSphere(transform.position, radius, freeLayers, QueryTriggerInteraction.Ignore);

            foreach (Collider Collider in Colliders)
            {
                if (socketOwner != placedPart)
                {
                    if (Collider as TerrainCollider == null)
                        return OccupancyType.Busy;
                }
                else
                {
                    if (!socketOwner.HasCollider(Collider) && Collider as TerrainCollider == null)
                        if (Collider.GetComponent<PartSocket>() != null || Collider.GetComponentInParent<PartSocket>() != null)
                            return OccupancyType.Busy;
                }
            }

            return OccupancyType.Free;
        }

        /// <summary>
        /// This method allows to check if the part is placed on this socket.
        /// </summary>
        public bool CheckOccupancy(Part part)
        {
            return busySpaces.Any(entry => entry.Type == part.Type);
        }

        /// <summary>
        /// This method allows to add an occupancy on this socket.
        /// </summary>
        public void AddOccupancy(Part part)
        {
            if (!CheckOccupancy(part))
            {
                if (part != socketOwner)
                {
                    busySpaces.Add(part);

                    for (int i = 0; i < part.OccupancyParts.Length; i++)
                        busySpaces.Add(part.OccupancyParts[i]);
                }
            }
        }

        /// <summary>
        /// This method allows to remove a occupancy from this socket.
        /// </summary>
        public void RemoveOccupancy(Part part)
        {
            if (CheckOccupancy(part))
            {
                busySpaces.Remove(busySpaces.Find(entry => entry == part));

                for (int i = 0; i < part.OccupancyParts.Length; i++)
                    busySpaces.Remove(busySpaces.Find(entry => entry == part.OccupancyParts[i]));
            }
        }

        /// <summary>
        /// This method allows to change the current socket state.
        /// </summary>
        public void ChangeState(OccupancyType state, Part part)
        {
            if (state == OccupancyType.Busy)
            {
                if (!CheckOccupancy(part))
                    AddOccupancy(part);
            }
            else if (state == OccupancyType.Free)
            {
                if (CheckOccupancy(part))
                    RemoveOccupancy(part);
            }
        }

        /// <summary>
        /// This method allows to change all around connected socket(s) state.
        /// </summary>
        public void ChangeAreaState(OccupancyType state, LayerMask freeLayer, LayerMask socketLayer, Part placedPart)
        {
            if (placedPart == null)
                return;

            if (placedPart.useConditionalPhysics)
                if (!placedPart.CheckStability())
                    return;

            Collider[] colliders = Physics.OverlapBox(placedPart.GetWorldPartMeshBounds().center,
                placedPart.GetWorldPartMeshBounds().extents,
                placedPart.transform.rotation, socketLayer, QueryTriggerInteraction.Collide);

            for (int i = 0; i < colliders.Length; i++)
            {
                PartSocket Socket = colliders[i].GetComponent<PartSocket>();

                if (Socket != null)
                {
                    if (state == OccupancyType.Busy)
                    {
                        if (Socket.AllowPart(placedPart) && Socket.CheckState(freeLayer, placedPart) == OccupancyType.Busy)
                        {
                            if (Vector3.Distance(placedPart.transform.position, Socket.transform.position) < placedPart.meshBounds.extents.magnitude)
                            {
                                placedPart.LinkPart(Socket.socketOwner);

                                Socket.ChangeState(OccupancyType.Busy, placedPart);
                            }
                        }
                    }
                    else
                    {
                        Socket.ChangeState(OccupancyType.Free, placedPart);
                    }
                }
            }

            colliders = Physics.OverlapSphere(transform.position, radius, freeLayer);

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].GetComponent<PartSocket>() == null)
                {
                    Part Part = colliders[i].GetComponentInParent<Part>();

                    if (Part != null)
                    {
                        if (Part != socketOwner)
                        {
                            if (AllowPart(Part))
                            {
                                ChangeState(OccupancyType.Busy, Part);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This method allows to get the part offset wich allowed on this socket.
        /// </summary>
        public PartOffset GetOffsetPart(int id)
        {
            for (int i = 0; i < PartOffsets.Count; i++)
                if (PartOffsets[i].Part != null && PartOffsets[i].Part.Id == id)
                    return PartOffsets[i];

            return null;
        }

        #endregion Public Methods



    }
}
