﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapCreatorUtility.Values
{
    public class Constants
    {
        #region Shaders
        public const string DIFFUSE_SHADER_NAME = "Diffuse";
        public const string TRANSPARENT_SHADER_NAME = "Easy Build System/Transparent (Optimized)";
        #endregion Shaders

        #region Layers
        public const string LAYER_DEFAULT = "Default";
        public const string LAYER_SOCKET = "socket";
        #endregion Layers

        #region PartsCollection
        public const string PARTS_COLLECTION_PATH = "Assets/Utilities/MapCreator/Resources/PartsCollections/";
        public const string PARTS_COLLECTION_RESOURCES_PATH = "PartsCollections/";
        public const string PARTS_COLLECTION_NAME = "DefaultPartsCollection";
        #endregion PartsCollection
    }
}
