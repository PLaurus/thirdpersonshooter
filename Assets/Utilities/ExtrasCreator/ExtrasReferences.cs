﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ExtrasReferences : MonoBehaviour {

	public GameObject modelInstance;
	public ExtrasAnimator extrasAnim;
	public GameObject itemModelInstance;

	public static ExtrasReferences singleton;

	void Awake(){
			singleton = this;
	}

	void Update(){
		if (singleton == null)
			singleton = this;
	}
}

#endif
