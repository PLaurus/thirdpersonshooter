﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class ExtrasCreator : MonoBehaviour {

	public bool loadModel;
	public GameObject modelPrefab;
	[Space]
	public bool createItem;
	[Space]
	public bool loadItem;
	public ItemInstance itemPrefab;
	[Space]
	public bool saveItem;

	ExtrasReferences exRef;

	ItemInstance curInstance;

	void Update(){
		if (exRef == null)
			exRef = ExtrasReferences.singleton;

		if (curInstance == null) {
			curInstance = GetComponent<ItemInstance> ();
			if (curInstance == null) {
				gameObject.AddComponent<ItemInstance> ();
				curInstance = GetComponent<ItemInstance> ();
			}
		}

		Buttons ();
	}

	void Buttons(){
		if (loadModel) {
			LoadModel ();
			loadModel = false;
		}

		if (createItem) {
			CreateItem ();
			createItem = false;
		}

		if (loadItem) {
			LoadItem ();
			loadItem = false;
		}

		if (saveItem) {
			SaveItem ();
			saveItem = false;
		}
	}

	void LoadModel(){
		if (modelPrefab == null) {
			Debug.Log ("No character model found");
			return;
		}

		if (exRef.modelInstance != null) {
			DestroyImmediate (exRef.modelInstance);
		}

		GameObject go = Instantiate (modelPrefab) as GameObject;
		exRef.modelInstance = go;
		go.AddComponent<ExtrasAnimator> ();
		exRef.extrasAnim = go.GetComponent<ExtrasAnimator> ();
		go.AddComponent<BoneHelpers> ();

		SceneView.RepaintAll ();
		EditorUtility.SetDirty (this);
		EditorUtility.SetDirty (exRef);
	}

	void CreateItem(){
		GameObject prefab = curInstance.instance.modelPrefab;
		if (prefab == null) {
			Debug.Log ("No Item prefab found! If this was intntional, add an empty gameobject as a plaseholder");
			return;
		}

		if (exRef.itemModelInstance != null)
			DestroyImmediate (exRef.itemModelInstance);

		exRef.itemModelInstance = Instantiate (prefab) as GameObject;

		Transform targetBone = exRef.extrasAnim.GetBoneHelper (curInstance.instance.bone).helper;
		exRef.itemModelInstance.transform.parent = targetBone;
		exRef.itemModelInstance.transform.localPosition = Vector3.zero;
		exRef.itemModelInstance.transform.localRotation = Quaternion.identity;
		targetBone.parent = null;

		SceneView.RepaintAll ();
		EditorUtility.SetDirty (this);
		EditorUtility.SetDirty (exRef);
	}

	void SaveItem(){
		if (exRef.itemModelInstance == null) {
			Debug.Log ("No item model instance found! Aborting save");
			return;
		}

		Item curI = curInstance.instance;
		curI.localPosition = exRef.itemModelInstance.transform.localPosition;
		curI.localEuler = exRef.itemModelInstance.transform.localEulerAngles;
		curI.localScale = exRef.itemModelInstance.transform.localScale;

		GameObject go = new GameObject ();
		go.name = curInstance.instance.itemId;
		go.AddComponent<ItemInstance> ();

		ItemInstance inst = go.GetComponent<ItemInstance> ();
		inst.instance = new Item ();
		Item newItem = inst.instance;

		Statics.CopyItemFromTo (ref newItem, curI);

		Debug.Log ("Item saved! Make a prefab from the new  object");

		SceneView.RepaintAll ();
		EditorUtility.SetDirty (this);
		EditorUtility.SetDirty (exRef);
	}

	void LoadItem(){
		if (itemPrefab == null) {
			Debug.Log ("No item to load!");
			return;
		}

		Item targetItem = itemPrefab.instance;
		Statics.CopyItemFromTo (ref curInstance.instance, targetItem);

		if (exRef.itemModelInstance != null)
			DestroyImmediate (exRef.itemModelInstance);

		GameObject prefab = curInstance.instance.modelPrefab;
		exRef.itemModelInstance = Instantiate (prefab) as GameObject;
		Transform targetBone = exRef.extrasAnim.GetBoneHelper (curInstance.instance.bone).helper;
		exRef.itemModelInstance.transform.parent = targetBone;
		exRef.itemModelInstance.transform.localPosition = curInstance.instance.localPosition;
		exRef.itemModelInstance.transform.localEulerAngles = curInstance.instance.localEuler;
		exRef.itemModelInstance.transform.localScale = curInstance.instance.localScale;
		targetBone.parent = null;
	}
}
#endif