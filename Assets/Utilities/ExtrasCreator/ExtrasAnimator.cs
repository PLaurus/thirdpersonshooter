﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ExtrasAnimator : MonoBehaviour {

	[HideInInspector]
	public Animator anim;
	public bool runAnimator;

	[HideInInspector]
	public BoneHelpers bHelper;

	void Update(){
		if (anim == null)
			anim = GetComponent<Animator> ();

		if (bHelper == null) {
			bHelper = GetComponent<BoneHelpers> ();
			bHelper.Init (anim);
		}

		bHelper.Tick ();

		if (runAnimator) {
			anim.Update (0);
		}
	}

	public BoneHelper GetBoneHelper(HumanBodyBones bone){
		if (anim == null)
			anim = GetComponent<Animator> ();

		if (bHelper == null) {
			bHelper = GetComponent<BoneHelpers> ();
			bHelper.Init (anim);
		}

		return bHelper.ReturnHelper (bone);
	}
}
#endif
