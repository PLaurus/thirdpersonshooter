﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class WeaponCreator : MonoBehaviour {
	public bool placeModels;
	public GameObject charModel;

	public bool initWeapon;
	public bool saveWeapon;
	public bool loadWeapon;
	public WeaponInstance instanceToLoad;

	public WeaponInstance weaponInstance;
	public bool saveNewWeaponState;
	public WeaponInstance saveStateToThisWeapon;
	public bool createMainIK;
	public bool createSecondaryIK;
	public bool initWeaponOnSecondary;
	WeaponCreatorReferences refs;
	public bool fpsCamera;

	void Update(){
		if (weaponInstance == null)
			weaponInstance = GetComponent<WeaponInstance> ();

		Buttons ();
	}

	void Buttons(){
		if (refs == null)
			refs = WeaponCreatorReferences.singleton;

		if (placeModels) {
			CreateModel ();
			placeModels = false;
		}

		if (initWeapon) {
			InitWeapon ();
			initWeapon = false;
		}

		if (saveWeapon) {
			SaveWeapon ();
			saveWeapon = false;
		}

		if (loadWeapon) {
			LoadWeapon ();
			loadWeapon = false;
		}

		if (initWeaponOnSecondary) {
			InitWeaponsOnSecondary ();
			initWeaponOnSecondary = false;
		}

		if (createMainIK) {
			CreateIkHandles (refs.animScripts [0]);
			createMainIK = false;
		}

		if (createSecondaryIK) {
			CreateSecondaryIK (refs.animScripts [1]);
			createSecondaryIK = false;
		}

		if (fpsCamera) {
			PlaceCameraToFps ();
			fpsCamera = false;
		}
	}

	void CreateModel(){
		foreach (GameObject go in refs.modelReferences) {
			if (go != null)
				DestroyImmediate (go);
		}

		for (int i = 0; i < 2; i++) {
			GameObject go = Instantiate (charModel) as GameObject;
			Vector3 targetPos = Vector3.zero;
			targetPos.x += -i * 2;
			go.transform.position = targetPos;
			refs.modelReferences [i] = go;
			Animator anim = go.GetComponent<Animator> ();
			anim.runtimeAnimatorController = refs.animOverrides [i];
			go.AddComponent<AnimatorEditor> ();
			refs.animScripts [i] = go.GetComponent<AnimatorEditor> ();
			go.AddComponent<BoneHelpers> ();
			refs.animScripts[i].bhelper = go.GetComponent<BoneHelpers>();
		}

		refs.animScripts [0].enableIK_RH = true;
		refs.animScripts [0].enableIK_LH = true;
		refs.animScripts [0].enableHeadTarget = true;

		//refs.animScripts [1].transform.position = -Vector3.right * 2;

		SceneView.RepaintAll ();
	}

	//this are for placing the IK positions when aiming
	void CreateIkHandles(AnimatorEditor targ){
		foreach (GameObject g in refs.ikHandles) {
			DestroyImmediate (g);
		}

		refs.ikHandles = new GameObject[2];

		if (refs.ikParent == null) {
			refs.ikParent = new GameObject ();
			refs.ikParent.name = "ik_shoulder_helper";
			if (targ != null) {
				refs.ikParent.transform.position = targ.anim.GetBoneTransform (HumanBodyBones.RightShoulder).position;
				Debug.Log (targ.anim.GetBoneTransform (HumanBodyBones.RightShoulder).position);
			}
		}

		if (refs.aimGuide != null)
			DestroyImmediate (refs.aimGuide);

		GameObject aimGprefab = Resources.Load ("AimGuide") as GameObject;
		GameObject aimG = Instantiate (aimGprefab) as GameObject;
		aimG.transform.localPosition = refs.ikParent.transform.position;
		refs.aimGuide = aimG;

		if (refs.headTarget != null)
			DestroyImmediate (refs.headTarget);

		refs.headTarget = GameObject.CreatePrimitive (PrimitiveType.Sphere) as GameObject;
		refs.headTarget.name = "head target";
		refs.headTarget.transform.parent = refs.ikParent.transform;
		refs.headTarget.transform.localPosition = Vector3.forward * 2 + -Vector3.up;
		refs.headTarget.transform.localScale = Vector3.one * 0.1f;

		for (int i = 0; i < 2; i++) {
			GameObject g = GameObject.CreatePrimitive (PrimitiveType.Capsule) as GameObject;
			g.transform.localScale = new Vector3 (0.05f, 0.09f, 0.05f);

			if (targ == null)
				g.transform.position = Vector3.forward + Vector3.up;
			else {
				if (i == 0) {
					g.transform.position = targ.anim.GetBoneTransform (HumanBodyBones.LeftHand).position;
					g.transform.name = "left_hand_target";
				} else {
					g.transform.position = targ.anim.GetBoneTransform (HumanBodyBones.RightHand).position;
					g.transform.name = "right_hand_target";
				}

				refs.ikHandles [i] = g;
			}

			DestroyImmediate(g.GetComponent<Collider>());
			refs.ikHandles [i].transform.parent = refs.ikParent.transform;
		}

		targ.headTarget = refs.headTarget.transform;
		targ.enableHeadTarget = true;
		targ.rh_target = refs.ikHandles [1].transform;
		targ.lh_target = refs.ikHandles [0].transform;
		targ.shoulder = refs.ikParent.transform;
		refs.animScripts [0].enableIK_RH = true;
		refs.animScripts [0].enableIK_LH = true;

		if (refs.fpsCameraPlacer != null)
			DestroyImmediate (refs.fpsCameraPlacer);

		refs.fpsCameraPlacer = GameObject.CreatePrimitive (PrimitiveType.Cube) as GameObject;
		DestroyImmediate (refs.fpsCameraPlacer.GetComponent<Collider> ());
		refs.fpsCameraPlacer.transform.localScale = Vector3.one * 0.02f;
		refs.fpsCameraPlacer.transform.parent = refs.ikParent.transform;
		refs.fpsCameraPlacer.transform.localPosition = Vector3.zero;
		refs.fpsCameraPlacer.name = "fps camera placer";

		if (refs.bulletSpawner != null)
			DestroyImmediate (refs.bulletSpawner);

		refs.bulletSpawner = GameObject.CreatePrimitive (PrimitiveType.Cube) as GameObject;
		DestroyImmediate (refs.bulletSpawner.GetComponent<Collider> ());
		refs.bulletSpawner.transform.localScale = Vector3.one * 0.02f;
		refs.bulletSpawner.transform.parent = refs.ikParent.transform;
		refs.bulletSpawner.transform.localPosition = Vector3.forward;
		refs.bulletSpawner.name = "bullet spawner";

		SceneView.RepaintAll ();
		EditorUtility.SetDirty (refs);
	}

	//this are for placing the IK positions while on idle animation
	void CreateSecondaryIK(AnimatorEditor targ){
		foreach (GameObject g in refs.secondaryIKs) {
			DestroyImmediate (g);
		}

		if (refs.secondaryParent == null) {
			refs.secondaryParent = new GameObject ();
			refs.secondaryParent.name = "secondary IKs";
			if (targ != null) {
				refs.secondaryParent.transform.position = targ.anim.GetBoneTransform (HumanBodyBones.RightShoulder).position;
			}
		}

		if (refs.secondaryIKs == null)
			refs.secondaryIKs = new GameObject[2];

		for (int i = 0; i < 2; i++) {
			GameObject g = GameObject.CreatePrimitive (PrimitiveType.Capsule) as GameObject;
			g.transform.localScale = new Vector3 (0.05f, 0.09f, 0.05f);

			if (targ == null)
				g.transform.position = Vector3.forward + Vector3.up;
			else {
				if (i == 0) {
					g.transform.position = targ.anim.GetBoneTransform (HumanBodyBones.LeftHand).position;
					g.transform.name = "left_hand_target";
				} else {
					g.transform.position = targ.anim.GetBoneTransform (HumanBodyBones.RightHand).position;
					g.transform.name = "right_hand_target";
				}
			}
			refs.secondaryIKs [i] = g;
			g.transform.parent = refs.secondaryParent.transform;
			DestroyImmediate(g.GetComponent<Collider>());
		}

		targ.lh_target = refs.secondaryIKs [0].transform;
		targ.enableIK_LH = true;
	}

	void SaveWeapon(){
		Weapon w = new Weapon ();

		if (weaponInstance == null)
			weaponInstance = GetComponent<WeaponInstance> ();

		Weapon weapon = weaponInstance.instance;

		w.weaponId = weapon.weaponId;
		w.modelPrefab = weapon.modelPrefab;

		WeaponStats ws = w.weaponStats;

		//Save ik Pos
		ws.aimguide_offset = refs.aimGuide.transform.position - refs.animScripts[0].anim.GetBoneTransform(HumanBodyBones.RightShoulder).position;
		ws.mainHand_pos = refs.ikHandles [1].transform.localPosition;
		ws.mainHand_rot = refs.ikHandles [1].transform.localEulerAngles;
		ws.offHand_pos_aim = refs.ikHandles [0].transform.localPosition;
		ws.offHand_rot_aim = refs.ikHandles [0].transform.localEulerAngles;

		Transform previous_parent = refs.secondaryIKs [0].transform.parent;
		Transform b = refs.animScripts [1].bhelper.ReturnHelper (HumanBodyBones.RightHand).helper;
		refs.secondaryIKs [0].transform.parent = b;
		ws.offHand_pos_idle = refs.secondaryIKs [0].transform.localPosition;
		ws.offHand_rot_idle = refs.secondaryIKs [0].transform.localEulerAngles;
		refs.secondaryIKs [0].transform.parent = previous_parent;

		ws.headTargetPos = refs.headTarget.transform.localPosition;
		ws.bulletSpawnPosition = refs.bulletSpawner.transform.localPosition;
		ws.fps_camera_offset = refs.fpsCameraPlacer.transform.localPosition;

		WeaponStats nws = new WeaponStats ();
		Statics.CopyWeaponStatsFromWeaponInstance (ref nws, ws);

		//Make a backup object, user must make prefab manually
		GameObject go = new GameObject();
		go.name = w.weaponId;
		go.AddComponent<WeaponInstance> ();
		WeaponInstance wi = go.GetComponent<WeaponInstance> ();
		wi.instance = w;

		//save model positions
		w.modelPrefab = weapon.modelPrefab;
		w.modelPos = refs.weaponModelInstance.transform.localPosition;
		w.modelRot = refs.weaponModelInstance.transform.localEulerAngles;
		w.modelScale = refs.weaponModelInstance.transform.localScale;
		w.weaponAnimSet = weapon.weaponAnimSet;

		//save holster positions
		w.hasHolster = weapon.hasHolster;
		w.holsterPrefab = weapon.holsterPrefab;
		w.holsterBone = weapon.holsterBone;

		if (refs.holsterModelInstance) {
			w.holsterPos = refs.holsterModelInstance.transform.localPosition;
			w.holsterRot = refs.holsterModelInstance.transform.localEulerAngles;
			w.holsterScale = refs.holsterModelInstance.transform.localScale;
		}

		Debug.Log ("Saved Weapon");
	}

	void InitWeapon(){
		if (refs.weaponModelInstance != null)
			DestroyImmediate (refs.weaponModelInstance);

		GameObject modelInstance = Instantiate (weaponInstance.instance.modelPrefab) as GameObject;
		refs.animScripts [0].bhelper.PlaceHelperOnBone (HumanBodyBones.RightHand, refs.animScripts [0].anim);
		BoneHelper b = refs.animScripts [0].bhelper.ReturnHelper (HumanBodyBones.RightHand);

		modelInstance.transform.rotation = Quaternion.Euler (Vector3.forward);
		modelInstance.transform.parent = b.helper;
		modelInstance.transform.localPosition = Vector3.zero;
		refs.weaponModelInstance = modelInstance;

		if (refs.secondaryModelInstances != null)
			DestroyImmediate (refs.secondaryModelInstances);

		GameObject secModelInstance = Instantiate (weaponInstance.instance.modelPrefab) as GameObject;
		refs.animScripts [1].bhelper.PlaceHelperOnBone (HumanBodyBones.RightHand, refs.animScripts [1].anim);
		BoneHelper b2 = refs.animScripts [1].bhelper.ReturnHelper (HumanBodyBones.RightHand);

		secModelInstance.transform.rotation = Quaternion.Euler (Vector3.forward);
		secModelInstance.transform.parent = b2.helper;
		secModelInstance.transform.localPosition = Vector3.zero;
		refs.secondaryModelInstances = secModelInstance;

		if (weaponInstance.instance.hasHolster) {
			if (refs.holsterModelInstance != null)
				DestroyImmediate (refs.holsterModelInstance);

			GameObject prefab = weaponInstance.instance.modelPrefab;
			if (weaponInstance.instance.holsterPrefab)
				prefab = weaponInstance.instance.holsterPrefab;

			GameObject holsterInstance = Instantiate (prefab) as GameObject;
			refs.animScripts [0].bhelper.PlaceHelperOnBone (weaponInstance.instance.holsterBone, refs.animScripts [0].anim);
			BoneHelper holsterBone = refs.animScripts [0].bhelper.ReturnHelper (weaponInstance.instance.holsterBone);

			holsterInstance.transform.rotation = Quaternion.Euler (-Vector3.up);
			holsterInstance.transform.parent = holsterBone.helper;
			holsterInstance.transform.localPosition = Vector3.zero;
			refs.holsterModelInstance = holsterInstance;
		}
	}

	void InitWeaponsOnSecondary(){
		refs.secondaryModelInstances.transform.localPosition = refs.weaponModelInstance.transform.localPosition;
		refs.secondaryModelInstances.transform.localRotation = refs.weaponModelInstance.transform.localRotation;
		refs.secondaryModelInstances.transform.localScale = refs.weaponModelInstance.transform.localScale;
		refs.animScripts [1].PlayAnim (weaponInstance.instance.weaponAnimSet);
	}

	void LoadWeapon(){
		AnimatorEditor aim = refs.animScripts [0];
		weaponInstance.instance = instanceToLoad.instance;

		if (refs.ikHandles [0] == null || refs.ikParent == null) {
			CreateIkHandles (aim);
		}

		Weapon w = weaponInstance.instance;

		BoneHelper b = refs.animScripts [0].bhelper.ReturnHelper (HumanBodyBones.RightHand);
		Transform prevParent = refs.ikHandles [0].transform.parent;
		refs.ikHandles [0].transform.parent = b.helper;
		refs.ikHandles [0].transform.localPosition = w.weaponStats.offHand_pos_idle;
		refs.ikHandles [0].transform.localEulerAngles = w.weaponStats.offHand_rot_idle;
		refs.ikHandles [0].transform.parent = prevParent;

		refs.animScripts [1].PlayAnim (w.weaponAnimSet);

		BoneHelper b2 = refs.animScripts [1].bhelper.ReturnHelper (HumanBodyBones.RightHand);
		Transform prevParent2 = refs.ikHandles [1].transform.parent;
		refs.secondaryIKs [0].transform.parent = b2.helper;
		refs.secondaryIKs [0].transform.localPosition = w.weaponStats.offHand_pos_idle;
		refs.secondaryIKs [0].transform.localEulerAngles = w.weaponStats.offHand_rot_idle;
		refs.secondaryIKs [0].transform.parent = prevParent2;

		refs.ikHandles [1].transform.localPosition = w.weaponStats.mainHand_pos;
		refs.ikHandles [1].transform.localEulerAngles = w.weaponStats.mainHand_rot;

		InitWeapon ();
		refs.weaponModelInstance.transform.localPosition = w.modelPos;
		refs.weaponModelInstance.transform.localEulerAngles = w.modelRot;
		refs.weaponModelInstance.transform.localScale = w.modelScale;
		refs.secondaryModelInstances.transform.localPosition = w.modelPos;
		refs.secondaryModelInstances.transform.localEulerAngles = w.modelRot;
		refs.secondaryModelInstances.transform.localScale = w.modelScale;

		if (refs.holsterModelInstance) {
			refs.holsterModelInstance.transform.localPosition = w.holsterPos;
			refs.holsterModelInstance.transform.localEulerAngles = w.holsterRot;
			refs.holsterModelInstance.transform.localScale = w.holsterScale;
		}

		Vector3 shoulderPos = refs.animScripts [0].bhelper.ReturnHelper (HumanBodyBones.RightShoulder).helper.position;
		shoulderPos += w.weaponStats.aimguide_offset;
		refs.aimGuide.transform.position = shoulderPos;
		refs.fpsCameraPlacer.transform.localPosition = w.weaponStats.fps_camera_offset;
		refs.bulletSpawner.transform.localPosition = w.weaponStats.bulletSpawnPosition;

		SceneView.RepaintAll ();
	}

	void PlaceCameraToFps(){
		if (refs.fpsCameraPlacer == null)
			return;

		Transform cam = Camera.main.transform;
		cam.transform.position = refs.fpsCameraPlacer.transform.position;
		cam.transform.rotation = Quaternion.identity;
		Camera.main.nearClipPlane = 0.1f;
		SceneView.RepaintAll ();
	}


}
