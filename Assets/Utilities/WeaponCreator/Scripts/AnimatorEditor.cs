﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AnimatorEditor : MonoBehaviour {

	public bool enableIK_RH = true;
	public bool enableIK_LH = true;
	public bool enableHeadTarget = true;
	public BoneHelpers bhelper;
	public Animator anim;
	public Transform rh_target;
	public Transform lh_target;
	public Transform shoulder;
	public Transform headTarget;
	WeaponCreatorReferences refs;

	void Update(){
		if (refs == null)
			refs = WeaponCreatorReferences.singleton;

		if (anim == null) {
			anim = GetComponent<Animator> ();
		}

		anim.Update (0);

		if (bhelper == null)
			bhelper = GetComponent<BoneHelpers> ();

		bhelper.Tick ();
	}

	void OnAnimatorMove(){
		if (anim == null)
			anim = GetComponent<Animator> ();

		HandleShoulderPosition ();
	}

	public void HandleShoulderPosition(){
		if (refs.aimGuide == null)
			return;

		Transform animShoulder = anim.GetBoneTransform (HumanBodyBones.RightShoulder);
		if (animShoulder == null)
			return;
		if (shoulder == null)
			return;
		
		Vector3 offsetPosition = refs.aimGuide.transform.position - animShoulder.position;
		shoulder.transform.position = animShoulder.position + offsetPosition;
		shoulder.transform.LookAt (Vector3.forward + shoulder.transform.position);
		refs.aimGuide.transform.LookAt (Vector3.forward + refs.aimGuide.transform.position);
	}

	public void PlayAnim(int i){
		string animName = "rifle";
		if (i == 1) {
			animName = "pistol";
		}
		anim.Play (animName);
	}

	void OnAnimatorIK(){
		if (headTarget) {
			anim.SetLookAtWeight ((enableHeadTarget) ? 1 : 0, 0, 1, 1, 1);
			anim.SetLookAtPosition (headTarget.position);
		} else {
			anim.SetLookAtWeight (0, 1, 1, 1, 1);
		}

		if (rh_target) {
			anim.SetIKPosition (AvatarIKGoal.RightHand, rh_target.position);
			anim.SetIKPositionWeight (AvatarIKGoal.RightHand, (enableIK_RH) ? 1 : 0);
			anim.SetIKRotationWeight (AvatarIKGoal.RightHand, (enableIK_RH) ? 1 : 0);
			anim.SetIKRotation (AvatarIKGoal.RightHand, rh_target.rotation);
		}

		if (lh_target) {
			anim.SetIKPosition (AvatarIKGoal.LeftHand, lh_target.position);
			anim.SetIKPositionWeight (AvatarIKGoal.LeftHand, (enableIK_LH) ? 1 : 0);
			anim.SetIKRotationWeight (AvatarIKGoal.LeftHand, (enableIK_LH) ? 1 : 0);
			anim.SetIKRotation (AvatarIKGoal.LeftHand, lh_target.rotation);
		}
	}
}
#endif