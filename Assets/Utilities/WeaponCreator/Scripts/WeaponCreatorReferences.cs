﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WeaponCreatorReferences : MonoBehaviour {
	public GameObject[] modelReferences = new GameObject[2];
	public AnimatorOverrideController[] animOverrides = new AnimatorOverrideController[2];
	public AnimatorEditor[] animScripts = new AnimatorEditor[2];
	public GameObject ikParent;
	public GameObject[] ikHandles = new GameObject[4];
	public GameObject secondaryParent;
	public GameObject[] secondaryIKs = new GameObject[4];
	public GameObject weaponModelInstance;
	public GameObject secondaryModelInstances;
	public GameObject holsterModelInstance;
	public GameObject headTarget;
	public GameObject aimGuide;
	public GameObject bulletSpawner;
	public GameObject fpsCameraPlacer;

	void Update(){
		if (singleton == null)
			singleton = this;
	}

	public static WeaponCreatorReferences singleton;

	void Awake(){
		singleton = this;
	}
}
#endif
