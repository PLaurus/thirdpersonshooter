﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class TPSEditorHelper{

	/*
	 * Scene directories are case sensitive, if you change your folder structure/ rename your folders or scenes
	 * you need to update this too
	 * */

	[MenuItem("Third Person Shooter/Scenes/Start Scene")]
	static void LoadStartScene(){
		EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
		EditorSceneManager.OpenScene("Assets/Scenes/start_here.unity");
	}

	[MenuItem("Third Person Shooter/Scenes/Dependencies Scene")]
	static void LoadDependencies(){
		EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
		EditorSceneManager.OpenScene("Assets/Scenes/dependencies.unity");
	}

	[MenuItem("Third Person Shooter/Scenes/Level dependencies Scene")]
	static void LoadLevelDependencies(){
		EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
		EditorSceneManager.OpenScene("Assets/Scenes/level_dependencies.unity");
	}

	[MenuItem("Third Person Shooter/Scenes/Weapon Creator")]
	static void LoadWeaponCreator(){
		EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
		EditorSceneManager.OpenScene("Assets/Scenes/weapon_creator.unity");
	}

	[MenuItem("Third Person Shooter/Scenes/Extras Creator")]
	static void LoadExtrasCreator(){
		EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
		EditorSceneManager.OpenScene("Assets/Scenes/extras_creator.unity");
	}

	[MenuItem("Third Person Shooter/Scenes/Game Scenes/Test scene")]
	static void LoadTestScene(){
		EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
		EditorSceneManager.OpenScene("Assets/Scenes/test_scene.unity");
	}

	[MenuItem("Third Person Shooter/Scenes/Game Scenes/Test scene 2")]
	static void LoadTestScene2(){
		EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
		EditorSceneManager.OpenScene("Assets/Scenes/test_scene2.unity");
	}

	[MenuItem("Third Person Shooter/Scenes/Map Creator")]
	static void LoadMapCreator(){
		EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
		EditorSceneManager.OpenScene("Assets/Scenes/MapCreator.unity");
	}

}

#endif
