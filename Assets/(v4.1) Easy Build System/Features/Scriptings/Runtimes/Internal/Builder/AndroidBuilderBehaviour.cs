﻿using EasyBuildSystem.Runtimes.Extensions;
using EasyBuildSystem.Runtimes.Internal.Builder;
using EasyBuildSystem.Runtimes.Internal.Managers;
using UnityEngine;

[AddComponentMenu("Easy Build System/Features/Builders Behaviour/Android Builder Behaviour")]
public class AndroidBuilderBehaviour : BuilderBehaviour
{
    #region Public Methods

    public override void UpdateModes()
    {
        base.UpdateModes();

        if (BuildManager.Instance == null)
            return;

        if (BuildManager.Instance.PartsCollection == null)
        {
            Debug.LogWarning("<b><color=yellow>[Easy Build System]</color></b> : Empty Parts Collection in the component Build Manager -> Parts Collection.");

            return;
        }
    }

    #endregion Public Methods
}