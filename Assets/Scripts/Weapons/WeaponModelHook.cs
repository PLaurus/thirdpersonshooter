﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponModelHook : MonoBehaviour {

	Animator anim;
	ParticleSystem[] otherParticles;

	public GameObject magazineOnWeapon;
	[Header("the magazine object with physics")]
	public string magazinePrefab;
	[HideInInspector]
	public string weaponId;
	[Header("The magazine object on the character's hand")]
	public string magazineItemId;
	GameObject magazineItem;

	BoneHelpers boneHelper;
	string modelRig;

	bool createAudio = false;
	AudioSource audioSource;

	public StringHolder stringHolder;

	public void Init(BoneHelpers bh, string targetRig){
		modelRig = targetRig;
		anim = GetComponentInChildren<Animator> ();
		otherParticles = GetComponentsInChildren<ParticleSystem> ();
		boneHelper = bh;

		CreateMagazineInstance ();

		if (createAudio) {
			gameObject.AddComponent<AudioSource> ();
			audioSource = GetComponent<AudioSource> ();
			audioSource.playOnAwake = false;
		}
	}

	public void ThrowMagazine(){
		GameObject go = ObjectPool.singleton.RequestObject (magazinePrefab);
		if (go == null)
			return;

		go.transform.position = magazineOnWeapon.transform.position;

		Rigidbody rb = go.GetComponent<Rigidbody> ();

		if (rb == null)
			return;

		rb.velocity = Vector3.zero;
		Vector3 direction = transform.root.forward;
		rb.AddRelativeTorque ((-Vector3.forward * 5) + (Vector3.right * Statics.RandomFloat (-2, 2)));
		rb.AddForce (direction * 3, ForceMode.Impulse);
	}

	public void Fire(){
		if (anim != null) {
			if (anim.isInitialized) {
				anim.SetBool (Statics.shoot, true);
			}
		}

		foreach (ParticleSystem p in otherParticles) {
			p.Emit (1);
		}

		if (createAudio) {
			AudioFX fx = AudioManager.singleton.GetAudio (stringHolder.gunshot);
			audioSource.clip = fx.audioClip;
			audioSource.Play ();
		}
	}

	void CreateMagazineInstance(){
		if (magazineItemId == null)
			return;

		ItemInstance ic = ResourcesManager.singleton.GetItem (magazineItemId, modelRig);
		if (ic == null)
			return;

		magazineItem = Instantiate (ic.instance.modelPrefab) as GameObject;
		Transform b = boneHelper.ReturnHelper (ic.instance.bone).helper;
		magazineItem.transform.parent = b;
		magazineItem.transform.localPosition = ic.instance.localPosition;
		magazineItem.transform.localEulerAngles = ic.instance.localEuler;
		magazineItem.transform.localScale = ic.instance.localScale;
		magazineItem.SetActive (false);
	}

	public void OpenMagazineOnHand(){
		if (magazineItem)
			magazineItem.SetActive (true);
	}

	public void CloseMagazineOnHand(){
		if (magazineItem)
			magazineItem.SetActive (false);
	}
}

[System.Serializable]
public class StringHolder{
	public string reload;
	public string gunshot;
	public string handle;
}
