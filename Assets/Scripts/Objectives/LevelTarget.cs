﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTarget : MonoBehaviour {

	int timesHit;

	public KillTargets hook;

	public void Hit(){
		timesHit++;

		if (timesHit > 3) {
			gameObject.SetActive (false);

			if (hook.objsToTrack.Contains (this.gameObject)) {
				hook.objsToTrack.Remove (this.gameObject);
			}

			hook.CheckProgress ();

			RadarManager.singleton.RemoveObj (this.gameObject);	
		}
	}
}
