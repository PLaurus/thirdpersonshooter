﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToArea : ObjectiveReferences {

	public Transform UITarget;

	public override void StartBehaviour(){
		OpenObjects ();
		CloseObjects ();
		EnableUI ();
	}

	public override void FinishBehaviour(){
		OnFinish ();
	}

	void OnFinish(){
		//Add more behaviours if needed

		DisableUI ();
	}

	void EnableUI(){
		RadarManager.singleton.AddTrackObj (UITarget.gameObject, Color.blue);
		CanvasOverlay.singleton.goToTarget = UITarget;
		CanvasOverlay.singleton.goToObjective = true;
	}

	void DisableUI(){
		RadarManager.singleton.RemoveObj (UITarget.gameObject);
		CanvasOverlay.singleton.goToObjective = false;
		CanvasOverlay.singleton.goToIndicator.gameObject.SetActive (false);
	}
}
