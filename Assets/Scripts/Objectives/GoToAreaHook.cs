﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToAreaHook : MonoBehaviour {

	void OnTriggerEnter(Collider other){
		InputHandler ih = other.GetComponent<InputHandler> ();//only the player has inputHandler (it's a notice)
		if (ih != null) {
			LevelObjectives.singleton.FinishObjective ();
			gameObject.SetActive (false);
		}
	}
}
