﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitObjective : ObjectiveReferences {

	public bool counterIsActive;
	public float waitTime = 5;
	float timer;

	public override void StartBehaviour(){
		OpenObjects ();
		EnableCounter ();
	}

	void EnableCounter(){
		counterIsActive = true;
	}

	void Update(){
		if (counterIsActive) {
			timer += Time.deltaTime;
			if (timer > waitTime) {
				LevelObjectives.singleton.FinishObjective ();
				counterIsActive = false;
				gameObject.SetActive (false);
			}
		}
	}
}
