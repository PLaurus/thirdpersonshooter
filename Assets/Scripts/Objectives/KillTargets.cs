﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillTargets : ObjectiveReferences {

	public List<GameObject> objsToTrack = new List<GameObject>();

	public override void StartBehaviour(){
		//objsToTrack.AddRange (objectsToOpen);

		OpenObjects ();
		CloseObjects ();
		EnableUI ();
	}

	public override void FinishBehaviour(){
		OnFinish ();
	}

	void OnFinish(){
		foreach (GameObject go in objectsToOpen) {
			go.SetActive (false);
		}

		DisableUI ();
	}

	void EnableUI(){
		CanvasOverlay.singleton.goToIndicator.gameObject.SetActive (false);
		CanvasOverlay.singleton.goToObjective = false;

		RadarManager rad = RadarManager.singleton;

		for (int i = 0; i < objsToTrack.Count; i++) {
			rad.AddTrackObj (objsToTrack [i], Color.red);
		}
	}

	void DisableUI(){
		RadarManager rad = RadarManager.singleton;

		for (int i = 0; i < objsToTrack.Count; i++) {
			rad.RemoveObj(objsToTrack [i]);
		}

		CanvasOverlay.singleton.goToObjective = false;
	}

	public void CheckProgress(){
		if (objsToTrack.Count == 0) {
			LevelObjectives.singleton.FinishObjective ();
		}
	}
}
