﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLevel : ObjectiveReferences {

	public string nextLevel;

	public override void StartBehaviour(){
		CallToLoadLevel ();
	}

	void CallToLoadLevel(){
		SessionMaster.singleton.LoadLevel (nextLevel);
	}
}
