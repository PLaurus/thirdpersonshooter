﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveReferences : MonoBehaviour {
	public GameObject[] objectsToOpen;
	public GameObject[] objectsToClose;

	public void StartObjective(){
		StartBehaviour ();
	}

	public void FinishObjective(){
		FinishBehaviour ();
	}

	public virtual void StartBehaviour(){
		
	}

	public virtual void FinishBehaviour(){
		
	}

	public void OpenObjects(){
		foreach (GameObject go in objectsToOpen) {
			go.SetActive (true);
		}
	}

	public void CloseObjects(){
		foreach (GameObject go in objectsToClose) {
			go.SetActive (false);
		}
	}
}
