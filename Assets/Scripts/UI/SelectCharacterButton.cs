﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectCharacterButton : MonoBehaviour {

	[SerializeField]
	string charId;
	Text txt;

	public void Init(string id){
		txt = GetComponentInChildren<Text> ();
		txt.text = id;
		charId = id;
	}

	public void SelectCharacter(){
		MainMenuManager.singleton.targetCharId = charId;
		MainMenuManager.singleton.LoadCharacter ();
	}
}
