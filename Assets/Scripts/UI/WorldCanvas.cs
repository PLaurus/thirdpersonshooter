﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCanvas : MonoBehaviour {

	public static WorldCanvas singleton;

	public Transform coverText;
	public Transform vaultText;

	void Awake(){
		singleton = this;
	}
}
