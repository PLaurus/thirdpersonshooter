﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationUI : MonoBehaviour {

	public RectTransform mask;
	public float speed = 100;

	float width;

	void Start(){
		width = -mask.rect.width;
	}

	void Update(){
		AnimateMask ();
	}

	void AnimateMask(){
		float currentOffset = mask.offsetMax.x + Time.deltaTime * speed;

		if (currentOffset >= 0f)
			currentOffset = width;

		mask.offsetMax = new Vector2 (currentOffset, mask.offsetMax.y);
	}

	public static IEnumerator SlideMaskLeftToRight(RectTransform mask, float animationSpeed){
		float width = -mask.rect.width;
		mask.offsetMax = new Vector2 (width, mask.offsetMax.y);
		float currentOffset = mask.offsetMax.x;

		while(currentOffset < 0f){
			currentOffset = mask.offsetMax.x + Time.deltaTime * animationSpeed;

			mask.offsetMax = new Vector2 (currentOffset, mask.offsetMax.y);

			yield return new WaitForEndOfFrame();
		}
	}
}
