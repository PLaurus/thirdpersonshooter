﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableItem : MonoBehaviour {

	public virtual void PickupItem(StateManager st){
		gameObject.SetActive (false);
		CanvasOverlay.singleton.pickupTextObject.SetActive (false);
	}

	public virtual void OnHighlight(StateManager st){
		
	}
}
