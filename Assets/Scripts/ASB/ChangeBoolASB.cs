﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBoolASB : StateMachineBehaviour {
	public string boolName;
	public bool status;
	public bool resetOnExit;

	//OnStateEnter is called when a transition starts and state machine starts to evaluate
	public override void OnStateEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animator.SetBool (boolName, status);
	}

	public override void OnStateExit (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (resetOnExit)
			animator.SetBool (boolName, !status);
	}
}
