﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddVelocityASB : StateMachineBehaviour {
	public float life = 0.4f; //how many second you are going to land
	public float force = 6;
	public Vector3 direction;

	[Space]
	[Header("This overrides the direction")]
	public bool useTransformForward;
	public bool additive;
	[Tooltip("The velocity will be applied on Enter")]
	public bool onEnter;
	[Tooltip("The velocity will be applied on Exit")]
	public bool onExit;
	[Header("When ending appling velocity! Not animation state")]
	public bool onEndClampVelocity;
	[Header("Use this to tailor the force application")]
	public bool useForceCurve;
	public AnimationCurve forceCurve;
	[Header("Can this be interrupted?")]
	public bool canBeInterrupted;

	StateManager states;
	PlayerMovementHandler playerMovementHandler;

	public override void OnStateEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (onEnter) {
			if (useTransformForward && !additive)
				direction = animator.transform.forward;

			if (useTransformForward && additive)
				direction += animator.transform.forward;

			if (states == null)
				states = animator.transform.GetComponent<StateManager> ();

			if (!states.isPlayer)
				return;

			if (playerMovementHandler == null)
				playerMovementHandler = animator.transform.GetComponent<PlayerMovementHandler> ();

			playerMovementHandler.AddVelocity (direction, life, force, onEndClampVelocity, useForceCurve, forceCurve, canBeInterrupted);
		}
	}

	public override void OnStateExit (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (onExit) {
			if (useTransformForward && !additive)
				direction = animator.transform.forward;

			if (useTransformForward && additive)
				direction += animator.transform.forward;

			if (states == null)
				states = animator.transform.GetComponent<StateManager> ();

			if (!states.isPlayer)
				return;

			if (playerMovementHandler == null)
				playerMovementHandler = animator.transform.GetComponent<PlayerMovementHandler> ();

			playerMovementHandler.AddVelocity (direction, life, force, onEndClampVelocity, useForceCurve, forceCurve, canBeInterrupted);
		}
	}
		
}
