﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuAnimator : MonoBehaviour {
	Animator anim;
	BoneHelpers bHelper;
	Transform lh_ikHelper;

	string rigType;

	public void Init(string targetRig){
		rigType = targetRig;
		bHelper = GetComponent<BoneHelpers> ();
		anim = GetComponent<Animator> ();
		bHelper.Init (anim);
		LoadWeapons ();
		bHelper.ParentAllHelpers ();
	}

	void LoadWeapons(){
		PlayerProfile pf = SessionMaster.singleton.GetProfile ();
		ResourcesManager rm = ResourcesManager.singleton;

		if (string.IsNullOrEmpty (pf.mainWeapon)) {
			pf.mainWeapon = "m4a1";
		}
		
		if (string.IsNullOrEmpty (pf.secWeapon)) {
			pf.secWeapon = "b8";
		}

		WeaponInstance mw = rm.GetWeapon (pf.mainWeapon, rigType);
		WeaponInstance sw = rm.GetWeapon (pf.secWeapon, rigType);

		if (mw == null) {//in case we cant find weapon, fall back to a different one
			Debug.Log(pf.mainWeapon + " main weapon wasn't found for " + rigType + " rig type, assigning default");

			mw = rm.GetWeapon ("m4a1", rigType);
		}

		if (sw == null) {//in case we cant find weapon, fall back to a different one
			Debug.Log(pf.secWeapon + " secondary weapon wasn't found for " + rigType + " rig type, assigning default");

			sw = rm.GetWeapon ("b8", rigType);
		}

		if (mw == null) {
			Debug.Log ("default main weapon for " + rigType + " rig type, wasn't found! Fix this!");
		}

		if (sw == null) {
			Debug.Log ("default secondary weapon for " + rigType + "rig type, wasn't found! Fix this!");
		}

		CreateWeapon (mw.instance, false);
		//CreateWeapon (sw.instance, true);

		lh_ikHelper = new GameObject ().transform;
		lh_ikHelper.parent = bHelper.ReturnHelper (HumanBodyBones.RightHand).helper;
		lh_ikHelper.transform.localPosition = mw.instance.weaponStats.offHand_pos_idle;
		lh_ikHelper.transform.localEulerAngles = mw.instance.weaponStats.offHand_rot_idle;
	}

	void OnAnimatorIK(){
		if (anim != null && lh_ikHelper != null) {
			anim.SetIKPositionWeight (AvatarIKGoal.LeftHand, 1);
			anim.SetIKPosition (AvatarIKGoal.LeftHand, lh_ikHelper.transform.position);
			anim.SetIKRotationWeight (AvatarIKGoal.LeftHand, 1);
			anim.SetIKRotation (AvatarIKGoal.LeftHand, lh_ikHelper.transform.rotation);
		}
	}

	void CreateWeapon(Weapon wi, bool holster){
		GameObject go = Instantiate (wi.modelPrefab);

		if (!holster) {
			Transform b = bHelper.ReturnHelper (HumanBodyBones.RightHand).helper;
			go.transform.parent = b;
			go.transform.localPosition = wi.modelPos;
			go.transform.localEulerAngles = wi.modelRot;
			go.transform.localScale = wi.modelScale;
		} else {
			Transform b = bHelper.ReturnHelper (wi.holsterBone).helper;
			go.transform.parent = b;
			go.transform.localPosition = wi.holsterPos;
			go.transform.localEulerAngles = wi.holsterRot;
			go.transform.localScale = wi.holsterScale;
		}
	}
}
