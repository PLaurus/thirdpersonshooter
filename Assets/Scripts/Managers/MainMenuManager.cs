﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour {

	public Transform characterPlacer;

	[HideInInspector]
	public string targetCharId;
	GameObject charInstance;
	MainMenuAnimator mmAnim;
	public AnimatorOverrideController mainMenuAnimator;

	void Start(){
		//Load settings
		UIManager.singleton.nameText.text = SessionMaster.singleton.GetProfile().playerName;
		targetCharId = SessionMaster.singleton.GetProfile ().charId;
		UIManager.singleton.nameInputField.text = SessionMaster.singleton.GetProfile ().playerName;
		LoadCharacter ();
	}

	public void LoadCharacter(){
		if (charInstance != null)
			Destroy (charInstance);

		CharContainer charContainer = ResourcesManager.singleton.GetChar (targetCharId);
		charInstance = Instantiate (charContainer.prefab) as GameObject;
		charInstance.transform.position = characterPlacer.position;
		charInstance.transform.rotation = characterPlacer.rotation;
		charInstance.GetComponent<Animator> ().runtimeAnimatorController = mainMenuAnimator;
		SessionMaster.singleton.GetProfile ().charId = targetCharId;

		charInstance.AddComponent<BoneHelpers> ();
		charInstance.AddComponent<MainMenuAnimator> ();
		mmAnim = charInstance.GetComponent<MainMenuAnimator> ();
		mmAnim.Init (charContainer.rig);
	}

	public static MainMenuManager singleton;
	void Awake(){
		singleton = this;
	}


}
