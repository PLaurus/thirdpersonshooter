﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public GameObject menuCanvas;
	public GameObject mainMenuUI;
	public GameObject gameMenuUI;
	public GameObject logo;
	public GameObject profileUI;
	public GameObject nameObject;
	public GameObject soloMenu;
	 
	public Text nameText;
	public InputField nameInputField;

	public Transform characterSelectGrid;

	void Start(){
		ResourcesManager rm = ResourcesManager.singleton;
		GameObject scbPrefab = Resources.Load ("selectCharacterButton") as GameObject;

		for (int i = 0; i < rm.charPrefabs.Count; i++) {
			GameObject go = Instantiate (scbPrefab) as GameObject;
			SelectCharacterButton sc = go.GetComponent<SelectCharacterButton> ();
			sc.Init (rm.charPrefabs [i].charId);
			go.transform.SetParent (characterSelectGrid);
		}

		logo.SetActive (false);
		mainMenuUI.SetActive (false);
		profileUI.SetActive (false);
		soloMenu.SetActive (false);
		nameObject.SetActive (false);
	}

	public void ProfileMenu(){
		soloMenu.SetActive (false);
		gameMenuUI.SetActive (false);
		mainMenuUI.SetActive (false);
		profileUI.SetActive (true);
		logo.SetActive (false);
		nameObject.SetActive (false);
	}

	public void FromGameToMenu(){// from game menu to main menu
		SessionMaster.singleton.LoadLevel ("menu");
	}

	public void FromMenuToSolo(){
		soloMenu.SetActive (true);
		gameMenuUI.SetActive (false);
		mainMenuUI.SetActive (false);
		profileUI.SetActive (false);
		logo.SetActive (false);
		nameObject.SetActive (false);
	}

	public void BackToMenu(){
		logo.SetActive (true);
		mainMenuUI.SetActive (true);
		profileUI.SetActive (false);
		nameObject.SetActive (true);
		gameMenuUI.SetActive (false);
		soloMenu.SetActive (false);
	}

	public void NewGame(){
		PlayerProfile p = SessionMaster.singleton.GetProfile ();
		p.currentLevel = "test_scene2";
		LoadScene ();
	}

	public void LoadScene(){
		PlayerProfile p = SessionMaster.singleton.GetProfile ();
		SessionMaster.singleton.loadingFromMenu = true;
		SessionMaster.singleton.LoadLevel (p.currentLevel);
	}

	public void OnWriteName(){
		SessionMaster.singleton.GetProfile ().playerName = nameInputField.text;
		nameText.text = nameInputField.text;
	}

	public void SaveProfileHook(){
		SessionMaster.singleton.SaveProfile ();
	}

	public static UIManager singleton;

	void Awake(){
		singleton = this;
		DontDestroyOnLoad (gameObject);
	}
}
